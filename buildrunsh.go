package main

/*
 * This program clones/pulls, builds, and runs a project solution. This
 * program is meant to be installed as a shell on each user's personal VM, as
 * it reads its input from stdin. The Aquinas Git server uses SSH to establish
 * an authenticated and encrypted connection to provide such input and trigger a
 * clone/pull, build, and run cycle.
 *
 * NOTE: This is to run in the context of the user who submitted the solution.
 * Therefore, this cannot be trusted to grade the submission; that must be
 * left to the program on the other side of the SSH connection because it
 * runs in another context. This program merely provides the submission's output
 * to the other.
 */

import (
	"bufio"
	"bytes"
	"encoding/json"
	"errors"
	"log/syslog"
	"os"
	"os/user"
	"path"
	"regexp"
	"strings"
	"time"
)

var logger  *syslog.Writer
var exitCode int

/* Ensure solution avoids keywords forbidden by project. */
func checkForbidden(proj projectInterface) (err error) {
	var srcFile    string
	var f         *os.File
	var blacklist *regexp.Regexp

	srcFile = proj.name() + langToExt(proj.lang())

	f, err = os.Open(srcFile)
	if err != nil {
		os.Stderr.WriteString("system error: buildrun: could " +
				      "not open " + srcFile + "\n")
		return
	}

	blacklist, err = regexp.Compile(proj.forbidden())
	if err != nil {
		os.Stderr.WriteString("system error: buildrun: could " +
				      "not compile " + proj.forbidden() + "\n")
		return
	}

	loc := blacklist.FindReaderIndex(bufio.NewReader(f))
	if loc != nil {
		buf := make([]byte, loc[1] - 1)

		f.ReadAt(buf, int64(loc[0]))

		msg := "found forbidden keyword " + string(buf) +
		       " in " + srcFile
		os.Stderr.WriteString(msg + "\n")
		err = errors.New(msg)
		return
	}

	return
}

func runChecks(proj projectInterface) (results results, err error) {
	for _, check := range proj.checks() {
		var checkExitCode int
		var errorMsg []byte

		cmd := strings.Fields(check.Command)
		if cmd, err = proj.applyRuntimeAltmain(cmd); err != nil {
			os.Stderr.WriteString("system error: buildrun: " + err.Error())
			return
		}

		output, errorMsg, err := runTimed(5 * time.Second,
		                                  bytes.NewReader(check.Stdin),
		                                  nil,
		                                  cmd[0],
		                                  cmd[1:]...)
		if err != nil {
			if stderrErr, ok := err.(*stderrError); ok {
				checkExitCode = stderrErr.exitCode
			} else if _, ok := err.(*os.PathError); ok {
				/*
				 * See StartProcess GoDoc and
				 * http://tldp.org/LDP/abs/html/exitcodes.html.
				 */
				checkExitCode = 126
			} else {
				logger.Debug(err.Error())
			}
		}

		results.Results = append(results.Results,
		                         result{check.Command,
		                                output,
		                                errorMsg,
		                                checkExitCode})
	}

	return
}

func buildCheck(student *user.User, proj projectInterface, submissionName string) (results results, err error) {
	dir := path.Join(student.HomeDir, submissionName)
	if err = os.Chdir(dir); err != nil {
		os.Stderr.WriteString("system error: buildrun: could not enter " + dir + "\n")
		return
	}

	if proj.forbidden() != "" {
		if err = checkForbidden(proj); err != nil {
			return
		}
	}

	if err = proj.build(); err != nil {
		return
	}

	if results, err = runChecks(proj); err != nil {
		return
	}

	return
}

func main() {
	var err error

	logger, err = syslog.New(syslog.LOG_NOTICE, os.Args[0])
	if err != nil {
		os.Stderr.WriteString("system error: buildrun: could not initialize logging\n")
		panic(err)
	}

	logger.Debug("executing")

	proj, err := decodeLanguageThenProject(os.Stdin)
	if err != nil {
		os.Stderr.WriteString("system error: buildrun: could not decode project description\n")
		fail(logger.Err, err.Error())
	}

	submissionName := proj.name() + proj.lang()

	logger.Debug("testing project " + submissionName)

	student, err := user.Current()
	if err != nil {
		os.Stderr.WriteString("system error: buildrun: could not decode determine current user\n")
		fail(logger.Err, err.Error())
	}

	logger.Debug("student is " + student.Username)

	results, err := buildCheck(student, proj, submissionName)
	if err != nil {
		exitCode = 1
		logger.Err(err.Error())
	}

	/* NOTE: Grader fills in results.Hash. */

	b, err := json.Marshal(results)
	if err != nil {
		os.Stderr.WriteString("system error: buildrun: could not encode results\n")
		fail(logger.Err, err.Error())
	}

	os.Stdout.Write(b)

	dir := path.Join(student.HomeDir, submissionName)

	if _, err = os.Stat(dir); os.IsNotExist(err) {
		return
	}

	if err := os.RemoveAll(dir); err != nil {
		os.Stderr.WriteString("system error: buildrun: could not remove submission\n")
	}

	logger.Debug("completed run of " + submissionName + " submission from " +
	              student.Username)

	os.Exit(exitCode)
}
