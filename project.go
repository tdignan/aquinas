package main

import (
	"errors"
	"os"
)

type project struct {
	Name           string          `json:"name"`
	Summary        string          `json:"summary"`
	Languages      []string        `json:"languages"`
	Prerequisites  []string        `json:"prerequisites"`
	Forbidden      string          `json:"forbidden"`
	Altmain        map[string]bool `json:"altmain"`
	Checks         []check         `json:"checks"`
	ServiceProgram string          `json:"service_program"`
	ServicePort    int             `json:"service_port"`
	ServiceBinary  bool            `json:"service_binary"`
}

type projectNone   project
type projectC      project
type projectAmd64  project
type projectGo     project
type projectPython project

type projectInterface interface {
	name() string
	summary() string
	languages() []string
	prerequisites() []string
	forbidden() string
	altmain() bool
	checks() []check
	serviceProgram() string
	servicePort() int
	lang() string
	build() error
	applyRuntimeAltmain(cmd []string) (cmd2 []string, err error)
}

func (proj projectNone) name() string { return proj.Name }
func (proj projectNone) summary() string { return proj.Summary }
func (proj projectNone) languages() []string { return proj.Languages }
func (proj projectNone) prerequisites() []string { return proj.Prerequisites }
func (proj projectNone) forbidden() string { return proj.Forbidden }
func (proj projectNone) altmain() bool { _, ok := proj.Altmain[proj.lang()]; return ok }
func (proj projectNone) checks() []check { return proj.Checks }
func (proj projectNone) serviceProgram() string { return proj.ServiceProgram }
func (proj projectNone) servicePort() int { return proj.ServicePort }
func (proj projectNone) lang() string { return "" }
func (proj projectNone) build() (err error) { return }
func (proj projectNone) applyRuntimeAltmain(cmd []string) (cmd2 []string, err error) { return cmd, err }

func (proj projectC) name() string { return proj.Name }
func (proj projectC) summary() string { return proj.Summary }
func (proj projectC) languages() []string { return proj.Languages }
func (proj projectC) prerequisites() []string { return proj.Prerequisites }
func (proj projectC) forbidden() string { return proj.Forbidden }
func (proj projectC) altmain() bool { _, ok := proj.Altmain[proj.lang()]; return ok }
func (proj projectC) checks() []check { return proj.Checks }
func (proj projectC) serviceProgram() string { return proj.ServiceProgram }
func (proj projectC) servicePort() int { return proj.ServicePort }
func (proj projectC) lang() string { return "C" }

func (proj projectC) build() (err error) {
	srcFile := proj.Name + langToExt(proj.lang())
	exeFile := proj.Name
	args := []string{"-o", exeFile}

	if proj.altmain() {
		args = append(args, "-Wl,-emain2", "main2"+langToExt(proj.lang()))
	}

	args = append(args, srcFile)

	if _, _, err = run(nil, nil, "gcc", args...); err != nil {
		os.Stderr.WriteString(err.Error() + "\n")
		err = errors.New(err.Error())
		return
	}

	return
}

func (proj projectC) applyRuntimeAltmain(cmd []string) (cmd2 []string, err error) { return cmd, err }

func (proj projectAmd64) name() string { return proj.Name }
func (proj projectAmd64) summary() string { return proj.Summary }
func (proj projectAmd64) languages() []string { return proj.Languages }
func (proj projectAmd64) prerequisites() []string { return proj.Prerequisites }
func (proj projectAmd64) forbidden() string { return proj.Forbidden }
func (proj projectAmd64) altmain() bool { _, ok := proj.Altmain[proj.lang()]; return ok }
func (proj projectAmd64) checks() []check { return proj.Checks }
func (proj projectAmd64) serviceProgram() string { return proj.ServiceProgram }
func (proj projectAmd64) servicePort() int { return proj.ServicePort }
func (proj projectAmd64) lang() string { return "AMD64" }

func (proj projectAmd64) build() (err error) {
	srcFile := proj.Name + langToExt(proj.lang())
	exeFile := proj.Name
	args := []string{"-s", "-nostartfiles", "-nostdlib", "-o", exeFile}

	args = append(args, srcFile)

	if _, _, err = run(nil, nil, "gcc", args...); err != nil {
		os.Stderr.WriteString(err.Error() + "\n")
		err = errors.New(err.Error())
		return
	}

	return
}

func (proj projectAmd64) applyRuntimeAltmain(cmd []string) (cmd2 []string, err error) { return cmd, err }

func (proj projectGo) name() string { return proj.Name }
func (proj projectGo) summary() string { return proj.Summary }
func (proj projectGo) languages() []string { return proj.Languages }
func (proj projectGo) prerequisites() []string { return proj.Prerequisites }
func (proj projectGo) forbidden() string { return proj.Forbidden }
func (proj projectGo) altmain() bool { _, ok := proj.Altmain[proj.lang()]; return ok }
func (proj projectGo) checks() []check { return proj.Checks }
func (proj projectGo) serviceProgram() string { return proj.ServiceProgram }
func (proj projectGo) servicePort() int { return proj.ServicePort }
func (proj projectGo) lang() string { return "Go" }

func (proj projectGo) build() (err error) {
	srcFile := proj.Name + langToExt(proj.lang())
	args := []string{"build", srcFile}

	if proj.altmain() {
		args = append(args, "main2"+langToExt(proj.lang()))
	}

	if _, _, err = run(nil, []string{"CGO_ENABLED=0"}, "go", args...); err != nil {
		os.Stderr.WriteString(err.Error() + "\n")
		err = errors.New(err.Error())
		return
	}

	return
}
func (proj projectGo) applyRuntimeAltmain(cmd []string) (cmd2 []string, err error) { return cmd, err }

func (proj projectPython) name() string { return proj.Name }
func (proj projectPython) summary() string { return proj.Summary }
func (proj projectPython) languages() []string { return proj.Languages }
func (proj projectPython) prerequisites() []string { return proj.Prerequisites }
func (proj projectPython) forbidden() string { return proj.Forbidden }
func (proj projectPython) altmain() bool { _, ok := proj.Altmain[proj.lang()]; return ok }
func (proj projectPython) checks() []check { return proj.Checks }
func (proj projectPython) serviceProgram() string { return proj.ServiceProgram }
func (proj projectPython) servicePort() int { return proj.ServicePort }
func (proj projectPython) lang() string { return "Python" }
func (proj projectPython) build() (err error) { return }
func (proj projectPython) applyRuntimeAltmain(cmd []string) (cmd2 []string, err error) {
	cmd2 = cmd
	if proj.altmain() && cmd[0] == "./" + proj.name() {
		cmd[0] = "./main2.py"

		if _, err = os.Stat(proj.name() + "Python.py"); os.IsNotExist(err) {
			if err = os.Rename(proj.name(), proj.name() + "Python.py"); err != nil {
				return cmd2, errors.New("could not rename " +
				                         proj.name() +
				                       " to functionsPython")
			}
		}
	}

	return
}
