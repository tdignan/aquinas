<?xml version="1.0" encoding="utf-8"?>
<!--
/=====================================================================\ 
|  LaTeXML-html5.xsl                                                  |
|  Stylesheet for converting LaTeXML documents to html5               |
|=====================================================================|
| Part of LaTeXML:                                                    |
|  Public domain software, produced as part of work done by the       |
|  United States Government & not subject to copyright in the US.     |
|=====================================================================|
| Bruce Miller <bruce.miller@nist.gov>                        #_#     |
| http://dlmf.nist.gov/LaTeXML/                              (o o)    |
\=========================================================ooo==U==ooo=/
-->
<xsl:stylesheet
    version   = "1.0"
    xmlns:xsl = "http://www.w3.org/1999/XSL/Transform"
    xmlns:ltx = "http://dlmf.nist.gov/LaTeXML"
    exclude-result-prefixes="ltx">

  <!-- Include all LaTeXML to xhtml modules -->
  <xsl:import href="urn:x-LaTeXML:XSLT:LaTeXML-all-xhtml.xsl"/>

  <!-- Override the output method & parameters -->
  <xsl:output
      method = "html"
      omit-xml-declaration="yes"
      encoding       = 'utf-8'
      media-type     = 'text/html'/>

  <!-- No namespaces; DO use HTML5 elements (include MathML & SVG) -->
  <xsl:param name="USE_NAMESPACES"  ></xsl:param>
  <xsl:param name="USE_HTML5"       >true</xsl:param>

  <xsl:template match="/" mode="doctype">
    <xsl:text disable-output-escaping='yes'>&lt;!DOCTYPE html></xsl:text>
  </xsl:template>

  <!-- Add <div>s. -->
  <xsl:template match="/" mode="body">
    <xsl:text>&#x0A;</xsl:text>
    <xsl:element name="body" namespace="{$html_ns}">
      <xsl:element name="div" namespace="{$html_ns}">
        <xsl:attribute name="class">page js-page</xsl:attribute>
        <xsl:apply-templates select="." mode="body-begin"/>
        <xsl:apply-templates select="." mode="navbar"/>
        <xsl:apply-templates select="." mode="body-main"/>
        <xsl:apply-templates select="." mode="body-end"/>
        <xsl:text>&#x0A;</xsl:text>
      </xsl:element>
    </xsl:element>
  </xsl:template>

  <!-- Add <div>s and handle title and so on here. -->
  <xsl:template match="/" mode="body-begin">
    <xsl:element name="div" namespace="{$html_ns}">
      <xsl:attribute name="class">header header-over large</xsl:attribute>
      <xsl:element name="div" namespace="{$html_ns}">
        <xsl:attribute name="class">container</xsl:attribute>

        <xsl:copy-of select="document('head.html')"/>
        <xsl:copy-of select="document('menu.html')"/>
      </xsl:element>
    </xsl:element>
    <xsl:element name="div" namespace="{$html_ns}">
      <xsl:attribute name="class">header-back header-back-simple header-back-small</xsl:attribute>
      <xsl:element name="div" namespace="{$html_ns}">
        <xsl:attribute name="class">header-back-container</xsl:attribute>
        <xsl:element name="div" namespace="{$html_ns}">
          <xsl:attribute name="class">container</xsl:attribute>
          <xsl:element name="div" namespace="{$html_ns}">
            <xsl:attribute name="class">row</xsl:attribute>
            <xsl:element name="div" namespace="{$html_ns}">
              <xsl:attribute name="class">col-md-12</xsl:attribute>
              <xsl:element name="div" namespace="{$html_ns}">
                <xsl:attribute name="class">page-info page-info-simple</xsl:attribute>
                <xsl:element name="h1" namespace="{$html_ns}">
                  <xsl:attribute name="class">page-title</xsl:attribute>
                  <xsl:value-of select="/ltx:document/ltx:title"/>
                </xsl:element>
                <xsl:element name="h2" namespace="{$html_ns}">
                  <xsl:attribute name="class">page-description</xsl:attribute>
                  <xsl:value-of select="/ltx:document/ltx:subtitle"/>
                </xsl:element>
              </xsl:element>
            </xsl:element>
          </xsl:element>
        </xsl:element>
      </xsl:element>
    </xsl:element>
  </xsl:template>

  <!-- Add <div>s. -->
  <xsl:template match="/" mode="body-main">
    <xsl:text>&#x0A;</xsl:text>
    <xsl:element name="div" namespace="{$html_ns}">
      <xsl:attribute name="class">container</xsl:attribute>
      <xsl:text>&#x0A;</xsl:text>
      <xsl:element name="div" namespace="{$html_ns}">
        <xsl:attribute name="class">main</xsl:attribute>
        <xsl:apply-templates select="." mode="body-main-begin"/>
        <xsl:apply-templates select="." mode="header"/>
        <xsl:apply-templates select="." mode="body-content"/>
        <xsl:apply-templates select="." mode="body-main-end"/>
      </xsl:element>
      <xsl:text>&#x0A;</xsl:text>
    </xsl:element>
  </xsl:template>

  <!-- Drop a <div>. -->
  <xsl:template match="/" mode="body-content">
    <xsl:text>&#x0A;</xsl:text>
    <xsl:apply-templates select="." mode="body-content-begin"/>
    <xsl:apply-templates/>
    <xsl:apply-templates select="." mode="body-content-end"/>
    <xsl:text>&#x0A;</xsl:text>
  </xsl:template>

  <!-- Include foot.html and js/main.js. -->
  <xsl:template match="/" mode="body-end">
    <xsl:copy-of select="document('foot.html')"/>
    <xsl:element name="script" namespace="{$html_ns}">
      <xsl:attribute name="src">js/main.js</xsl:attribute>
    </xsl:element>
  </xsl:template>

  <!-- Skip title, if top-level (xsl:if). -->
  <xsl:preserve-space elements="ltx:title"/>
  <xsl:template match="ltx:title">
    <xsl:param name="context"/>
    <xsl:if test="not(parent::ltx:document)">
      <xsl:text>&#x0A;</xsl:text>
      <xsl:call-template name="maketitle">
        <xsl:with-param name="context" select="$context"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

</xsl:stylesheet>
