package main

/*
 * This program creates and removes users on behalf of the Aquinas WWW
 * server. This program is meant to be installed as a shell on the Aquinas
 * Git server, as it reads its input from stdin. The Aquinas WWW server uses
 * SSH to establish an authenticated and encrypted connection to provide
 * such input.
 */

import (
	"encoding/base64"
	"fmt"
	"log/syslog"
	"os"
	"strings"
)

func main() {
	var op, arg string

	logger, err := syslog.New(syslog.LOG_NOTICE, os.Args[0])
	if err != nil {
		panic(err)
	}

	if _, err := fmt.Scanf("%s %s\n", &op, &arg); err != nil {
		fail(logger.Err, err.Error())
	}

	logger.Debug("executing " + op)

	switch op {
	case "ssh":
		student := arg

		output, _, err := enqueue("-s", "/usr/sbin/aquinas-get-ssh-authorized-keys", student)
		if err != nil {
			fail(logger.Err, err.Error())
		}

		fmt.Printf("%s", string(output))
	case "add":
		student := arg

		_, jobUuid, err := enqueue("/usr/sbin/aquinas-add-student-slave", student)
		if err != nil {
			fail(logger.Err, err.Error())
		}
		fmt.Printf("%s", string(jobUuid))
	case "remove":
		student := arg

		_, jobUuid, err := enqueue("/usr/sbin/aquinas-remove-student-slave", student)
		if err != nil {
			fail(logger.Err, err.Error())
		}
		fmt.Printf("%s", string(jobUuid))
	case "key":
		decoded, err := base64.StdEncoding.DecodeString(arg)
		if err != nil {
			fail(logger.Err, err.Error())
		}

		s := strings.Split(string(decoded), ":")
		student, key := s[0], s[1]

		_, jobUuid, err := enqueue("/usr/sbin/aquinas-deploy-key", student, key)
		if err != nil {
			fail(logger.Err, err.Error())
		}
		fmt.Printf("%s", string(jobUuid))
	default:
		fail(logger.Err, "illegal input to " + os.Args[0])
	}
}
