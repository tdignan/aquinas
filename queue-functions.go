package main

import (
	"encoding/json"
	"net"
	"strings"
	"syscall"
)

const SOCKPATH = "/tmp/queue-socket"

type perm struct {
	as string
	by string
}

var PERMISSIONS = map[string][]perm{
	/* Grader can be run as teacher by anyone. */
	"/usr/sbin/grader": []perm{{as: "teacher", by: ""}},

	/* Initialize projects can be run as root by teacher. */
	"/usr/sbin/aquinas-initialize-projects": []perm{{as: "root", by: "teacher"}},

	/* These can be run as root by http. */
	"/usr/sbin/aquinas-add-student-slave":       []perm{{as: "root", by: "http"}},
	"/usr/sbin/aquinas-get-ssh-authorized-keys": []perm{{as: "root", by: "http"}},
	"/usr/sbin/aquinas-deploy-key":              []perm{{as: "root", by: "http"}},
	"/usr/sbin/aquinas-remove-student-slave":    []perm{{as: "root", by: "http"}},
}

func checkPerm(program, by string) (as string, ok bool) {
	perms, ok := PERMISSIONS[program]
	if !ok {
		return "", false
	}

	for _, p := range(perms) {
		if p.by == "" || p.by == by {
			return p.as, true
		}
	}

	return "", false
}

func getCreds(c*net.UnixConn) (ucred *syscall.Ucred, err error) {
	f, err := c.File()
	if err != nil {
		return nil, err
	}
	defer f.Close()

	return syscall.GetsockoptUcred(int(f.Fd()),
	                               syscall.SOL_SOCKET,
	                               syscall.SO_PEERCRED)
}

func enqueue(args ...string) (stdout, stderr []byte, err error) {
	var buf [1024 * 10]byte
	var result result

	c, err := net.DialUnix("unix", nil, &net.UnixAddr{SOCKPATH, "unix"})
	if err != nil {
		return
	}
	defer c.Close()

	n, err := c.Write([]byte(strings.Join(args, "\x00") + "\n"))
	if err != nil {
		return
	}

	n, err = c.Read(buf[:])
	if err != nil {
		return
	}

	dec := json.NewDecoder(strings.NewReader(string(buf[:n])))
	if err = dec.Decode(&result); err != nil {
		return
	}

	return result.Stdout, result.Stderr, nil
}
