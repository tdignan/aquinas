#include <sys/types.h>
#include <errno.h>
#include <pwd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define PASSWD "/etc/passwd"
#define USER   "nobody"

void usage(void)
{
	fprintf(stderr, "usage: chrootdrop DIR CMD [ARG...]\n");
	exit(EXIT_FAILURE);
}

int main(int argc, char *argv[])
{
	int rc;
	FILE *pw;
	struct passwd *ent;

	if (argc <= 2) {
		usage();
	}

	pw = fopen(PASSWD, "r");	
	if (NULL == pw) {
		perror("could not open " PASSWD);
		exit(EXIT_FAILURE);
	}

	errno = 0;
	do {    /*
		 * Avoid getpwent as it requires shared libraries,
		 * and nobody is surely in /etc/passwd rather than
		 * LDAP, etc.
		 */
		ent = fgetpwent(pw);
	} while (ent != NULL && strcmp(ent->pw_name, USER));

	if (NULL == ent) {
		if (0 != errno) {
			perror("error looking up user nobody");
		} else {
			fprintf(stderr, "user nobody does not exist");
		}

		exit(EXIT_FAILURE);
	}

	fclose(pw);

	rc = chroot(argv[1]);
	if (rc) {
		perror("could not chroot");
		exit(EXIT_FAILURE);
	}

	rc = chdir("/");
	if (rc) {
		perror("could not chdir");
		exit(EXIT_FAILURE);
	}

	rc = setgid(ent->pw_gid);
	if (rc) {
		perror("could not setgid");
		exit(EXIT_FAILURE);
	}

	rc = setuid(ent->pw_uid);
	if (rc) {
		perror("could not setuid");
		exit(EXIT_FAILURE);
	}

	execve(argv[2], argv + 2, NULL);
	perror("could not exec");
	exit(EXIT_FAILURE);
}
