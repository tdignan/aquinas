package main

import (
	"bytes"
	"errors"
	"html/template"
	"io"
	"net/http"
	"net/mail"
	"net/smtp"
	"os"
	"path"
	"strings"
	textTemplate "text/template"
	"time"
)

var mimeMap = map[string]string{
	".css":   "text/css",
	".ico":   "image/x-icon",
	".jpg":   "image/jpeg",
	".js":    "ext/javascript",
	".png":   "image/png",
	".svg":   "image/svg+xml",
	".woff2": "font/woff2",
	".woff":  "font/woff",
}

/* Serves differently depending on if logged in or not. */
func (s *server) mirror(hUnauth http.HandlerFunc, hAuth HandlerFuncAuth) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		user, pass, ok := r.BasicAuth()
		if !ok {
			hUnauth(w, r)
		} else {
			if err := s.db.authenticate(user, pass); err != nil {
				logger.Notice(err.Error())

				serveLogin(w)
				return
			}

			/* Ask for and install SSH key on first login. */
			file := r.URL.Path[1:]
			sshAuthKeys, err := s.db.sshAuthKeys(user)
			if err != nil {
				s.handleError(w, err, "")
				return
			}

			if file != "ssh.html" && file != "ssh2.html" && file != "aquinas.css" && sshAuthKeys == "" {
				http.Redirect(w, r, "/ssh.html", http.StatusSeeOther)
				logger.Notice("forwarding from " + file + " to ssh.html")
				return
			}

			hAuth(w, r, user)
		}
	}
}

/* Serves if logged in; otherwise prompts for credentials. */
func (s *server) protect(h HandlerFuncAuth) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		user, pass, ok := r.BasicAuth()
		if !ok {
			serveLogin(w)
			return
		}

		if err := s.db.authenticate(user, pass); err != nil {
			logger.Notice(err.Error())

			serveLogin(w)
			return
		}

		/* Ask for and install SSH key on first login. */
		file := r.URL.Path[1:]
		sshAuthKeys, err := s.db.sshAuthKeys(user)
		if err != nil {
			s.handleError(w, err, "")
			return
		}

		if file != "ssh.html" && file != "ssh2.html" && file != "aquinas.css" && sshAuthKeys == "" {
			http.Redirect(w, r, "/ssh.html", http.StatusSeeOther)
			logger.Notice("forwarding from " + file + " to ssh.html")
			return
		}


		h(w, r, user)
	}
}

func (s *server) handleError(w http.ResponseWriter, err *dualError, msg string) {
	if msg != "" {
		http.Error(w, errorMap[err.errorPub()] + ": " + msg, err.errorPub())
		logger.Err(err.Error() + ": " + msg)
	} else {
		http.Error(w, errorMap[err.errorPub()], err.errorPub())
		logger.Err(err.Error())
	}
}

func (s *server) handleRestUnauth() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path == "/" {
			http.Redirect(w, r, "/landing.html", http.StatusSeeOther)
			logger.Notice("forwarding from / to landing.html")
			return
		}

		file := r.URL.Path[1:]

		if !strings.Contains(file, ".") {
			/* Assume this is a binary. */
			w.Header().Set("Content-Type", "application/octet-stream")

			p := path.Join(*root, file)
			http.ServeFile(w, r, p)
		} else {
			values := map[string]interface{}{
				"gitHost": gitHost(r.Host),
				"root": conf.Root,
			}

			if err := serve(w, r, file, values); err != nil {
				s.handleError(w, err, "")
			}
		}
	}
}

func (s *server) handleIndexUnauth() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, "/landing.html", http.StatusSeeOther)
		logger.Notice("forwarding from " + r.URL.Path[1:] + " to landing.html")
	}
}

func (s *server) handleProjectsUnauth() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if err := serve(w, r, "projects.html", nil); err != nil {
			s.handleError(w, err, "")
		}
	}
}

func (s *server) handleLoginUnauth() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		serveLogin(w)
	}
}

func (s *server) handleLanding() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if err := serve(w, r, "landing.html", nil); err != nil {
			s.handleError(w, err, "")
		}
	}
}

func (s *server) handleRegisterReset2(w http.ResponseWriter, r *http.Request, op string, f func(string) error) {
	var emailBodyFilled bytes.Buffer

	student, err := safeEmail(r.FormValue, "student")
	if err != nil {
		s.handleError(w, &dualError{err: err, errPub: http.StatusBadRequest}, "")
		return
	}

	if _, err = mail.ParseAddress(student); err != nil {
		msg := "ill-formed email addres"
		s.handleError(w, &dualError{err: err, errPub: http.StatusBadRequest}, msg)
		return
	}

	if !allowedEmailDomain(student) {
		msg := "disallowed email domain"
		s.handleError(w, &dualError{err: err, errPub: http.StatusBadRequest}, msg)
		return
	}

	dir := path.Join("/etc/httpd/accounts", student)
	if err := f(dir); err != nil {
		s.handleError(w, &dualError{err: err,
					    errPub: http.StatusBadRequest}, err.Error())
		return
	}

	p := path.Join(*root, "email-" + op)
	tmpl1, err := textTemplate.New(path.Base(p)).ParseFiles(p)
	if err != nil {
		s.handleError(w, &dualError{err: err, errPub: http.StatusInternalServerError}, "")
		return
	}

	uuid, err := uuid()
	if err != nil {
		s.handleError(w, &dualError{err: err, errPub: http.StatusInternalServerError}, "")
		return
	}

	pass, err := password()
	if err != nil {
		s.handleError(w, &dualError{err: err, errPub: http.StatusInternalServerError}, "")
		return
	}

	nonce := nonce()
	token := nonce + student + secret

	err = tmpl1.Execute(&emailBodyFilled, map[string]interface{}{
		"date": time.Now().Format(time.RFC1123Z),
		"from": conf.EmailSender,
		"fromName": conf.EmailSenderName,
		"host": r.Host,
		"nonce": nonce,
		"token": hashCalc(token),
		"student": student,
		"password": pass,
		"uuid": uuid})
	if err != nil {
		s.handleError(w, &dualError{err: err, errPub: http.StatusInternalServerError}, "")
		return
	}

	logger.Notice("preparing " + op + " email" +
		      " student: "  + student +
		      " nonce: "    + nonce +
		      " token: "    + hashCalc(token) +
		      " password: " + pass)

	if strings.HasSuffix(student, "example.com") {
		msg := "example.com; assuming test thus not sending email"
		err := errors.New("disallowed email domain")
		s.handleError(w, &dualError{err: err, errPub: http.StatusBadRequest}, msg)
		return
	}

	err = smtp.SendMail(conf.EmailRelay, nil, conf.EmailSender,
			  []string{student},
			  []byte(emailBodyFilled.Bytes()))
	if err != nil {
		s.handleError(w, &dualError{err: err, errPub: http.StatusInternalServerError}, "")
		return
	}

	p = path.Join(*root, op + "2.html")
	tmpl2, err := template.New(path.Base(p)).ParseFiles(p)
	if err != nil {
		s.handleError(w, &dualError{err: err, errPub: http.StatusInternalServerError}, "")
		return
	}

	w.Header().Set("Content-Type", "text/html")

	err = tmpl2.Execute(w, map[string]interface{}{"student": student})
	if err != nil {
		s.handleError(w, &dualError{err: err, errPub: http.StatusInternalServerError}, "")
		return
	}

	logger.Notice("not authenticated: provided " + op + "2.html")

	return
}

func (s *server) handleRegisterReset3(w http.ResponseWriter, r *http.Request, op string, f func(string, string) error) {
	p := path.Join(*root, op + "3.html")

	student, err := safeEmail(r.FormValue, "student")
	if err != nil {
		s.handleError(w, &dualError{err: err, errPub: http.StatusInternalServerError}, "")
		return
	}

	nonce, err := safe(r.FormValue, "nonce")
	if err != nil {
		s.handleError(w, &dualError{err: err, errPub: http.StatusInternalServerError}, "")
		return
	}

	hashedToken, err := safe(r.FormValue, "token")
	if err != nil {
		s.handleError(w, &dualError{err: err, errPub: http.StatusInternalServerError}, "")
		return
	}

	password, err := safe(r.FormValue, "password")
	if err != nil {
		s.handleError(w, &dualError{err: err, errPub: http.StatusInternalServerError}, "")
		return
	}

	token := nonce + student + secret
	if !hashSame(hashedToken, hashCalc(token)) {
		msg := "faulty authentication; is token expired?"
		s.handleError(w, &dualError{err: err, errPub: http.StatusBadRequest}, msg)
		return
	}

	logger.Notice("received valid authentication to " + op + "3")
	err = f(student, password)
	if err != nil {
		s.handleError(w, &dualError{err: err, errPub: http.StatusInternalServerError}, "")
		return
	}

	tmpl, err := template.New(path.Base(p)).ParseFiles(p)
	if err != nil {
		s.handleError(w, &dualError{err: err, errPub: http.StatusInternalServerError}, "")
		return
	}

	w.Header().Set("Content-Type", "text/html")

	err = tmpl.Execute(w, nil)
	if err != nil {
		s.handleError(w, &dualError{err: err, errPub: http.StatusInternalServerError}, "")
		return
	}

	logger.Notice("not authenticated: provided " + op + "3.html")

	return
}

func (s *server) handleRegister() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if err := serve(w, r, "register.html", nil); err != nil {
			s.handleError(w, err, "")
		}
	}
}

func (s *server) handleRegister2() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		s.handleRegisterReset2(w, r, "register", func(dir string) (err error) {
			if _, err = os.Stat(dir); err == nil {
				err = errors.New("student exists")
			} else {
				err = nil
			}
			return
		})
	}
}

func (s *server) handleRegister3() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		s.handleRegisterReset3(w, r, "register", func(student, password string) (err error) {
			_, err = s.db.addStudent(student, password)
			return
		})
	}
}

func (s *server) handleReset() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if err := serve(w, r, "reset.html", nil); err != nil {
			s.handleError(w, err, "")
		}
	}
}

func (s *server) handleReset2() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		s.handleRegisterReset2(w, r, "reset", func(dir string) (err error) {
			if _, err = os.Stat(dir); err != nil {
				err = errors.New("student does not exist")
			} else {
				err = nil
			}
			return
		})
	}
}

func (s *server) handleReset3() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		s.handleRegisterReset3(w, r, "reset", func(student, password string) (err error) {
			return s.db.setPassword(student, password)
		})
	}
}

func (s *server) handleStatic() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		mimeType, ok := mimeMap[path.Ext(r.URL.Path)]
		if ok {
			w.Header().Set("Content-Type", mimeType)
		} else {
			logger.Debug("unknown file extension: " + r.URL.Path)
			w.Header().Set("Content-Type", "application/octet-stream")
		}

		f, err := os.Open(path.Join(*root, r.URL.Path[1:]))
		if err != nil {
			s.handleError(w, &dualError{err: err, errPub: http.StatusNotFound}, "")
			return
		}

		_, err = io.Copy(w, f)
		if err != nil {
			s.handleError(w, &dualError{err: err, errPub: http.StatusInternalServerError}, "")
			return
		}
	}
}

func (s *server) handleRestAuth() HandlerFuncAuth {
	return func(w http.ResponseWriter, r *http.Request, user string) {
		if r.URL.Path == "/" {
			http.Redirect(w, r, "/projects.html", http.StatusSeeOther)
			logger.Notice("forwarding from / to projects.html")
			return
		}

		file := r.URL.Path[1:]

		if !strings.Contains(file, ".") {
			/* Assume this is a binary. */
			w.Header().Set("Content-Type", "application/octet-stream")

			p := path.Join(*root, file)
			http.ServeFile(w, r, p)
		} else {
			records := template.HTML(s.db.records(user, file))

			values := map[string]interface{}{
				"gitHost": gitHost(r.Host),
				"student": user,
				"last": s.db.last(user, file),
				"root": conf.Root,
				"records": records,
			}

			if err := serve(w, r, file, values); err != nil {
				s.handleError(w, err, "")
			}
		}
	}
}

func (s *server) handleIndexAuth() HandlerFuncAuth {
	return func(w http.ResponseWriter, r *http.Request, user string) {
		http.Redirect(w, r, "/projects.html", http.StatusSeeOther)
		logger.Notice("forwarding from " + r.URL.Path[1:] + " to projects.html")
	}
}

func (s *server) handleProjectsAuth() HandlerFuncAuth {
	return func(w http.ResponseWriter, r *http.Request, user string) {
		p := path.Join(*root, "projects.html")

		tmpl, err := template.New(path.Base(p)).ParseFiles(p)
		if err != nil {
			s.handleError(w, &dualError{err: err,errPub: http.StatusInternalServerError}, "")
			return
		}

		w.Header().Set("Content-Type", "text/html")

		result, err := s.db.attempts(user)
		if err != nil {
			s.handleError(w, &dualError{err: err, errPub: http.StatusInternalServerError}, "")
			return
		}

		err = tmpl.Execute(w, map[string]interface{}{
			"result": result,
			"student": user})
		if err != nil {
			s.handleError(w, &dualError{err: err, errPub: http.StatusInternalServerError}, "")
			return
		}
	}
}

func (s *server) handleLoginAuth() HandlerFuncAuth {
	return func(w http.ResponseWriter, r *http.Request, user string) {
		/* Post-authentication request following first load of login.html. */
		http.Redirect(w, r, "/", http.StatusSeeOther)
		logger.Notice("forwarding from login.html to projects.html")
	}
}

func (s *server) handleSsh() HandlerFuncAuth {
	return func(w http.ResponseWriter, r *http.Request, user string) {
		sshAuthKeys, err := s.db.sshAuthKeys(user)
		if err != nil {
			s.handleError(w, err, "")
			return
		}

		values := map[string]interface{}{
			"authorizedKeys": sshAuthKeys,
			"student": user,
		}

		if err := serve(w, r, "ssh.html", values); err != nil {
			s.handleError(w, err, "")
		}
	}
}

func (s *server) handleSsh2() HandlerFuncAuth {
	return func(w http.ResponseWriter, r *http.Request, user string) {
		key, err := safeKey(r.FormValue, "key")
		if err != nil {
			http.Error(w, "ill-formed key", http.StatusInternalServerError)
			logger.Err(err.Error())
			return
		}

		logger.Notice("received public key " + key)

		err = s.db.setSshAuthKeys(user, key)
		if err != nil {
			s.handleError(w, &dualError{err: err, errPub: http.StatusInternalServerError}, "")
			return
		}

		values := map[string]interface{}{
			"student": user,
		}

		if err := serve(w, r, "ssh2.html", values); err != nil {
			s.handleError(w, err, "")
			return
		}
	}
}

func (s *server) handlePassword() HandlerFuncAuth {
	return func(w http.ResponseWriter, r *http.Request, user string) {
		values := map[string]interface{}{
			"student": user,
		}

		if err := serve(w, r, "password.html", values); err != nil {
			s.handleError(w, err, "")
		}
	}
}

func (s *server) handlePassword2() HandlerFuncAuth {
	return func(w http.ResponseWriter, r *http.Request, user string) {
		password1 := r.FormValue("password1")
		if password1 == "" {
			msg := "missing password"
			http.Error(w, msg, http.StatusInternalServerError)
			logger.Err(msg)
			return
		}

		password2 := r.FormValue("password2")
		if password2 == "" {
			msg := "missing password confirmation"
			http.Error(w, msg, http.StatusInternalServerError)
			logger.Err(msg)
			return
		}

		if password1 != password2 {
			msg := "password entries do not match"
			http.Error(w, msg, http.StatusInternalServerError)
			logger.Err(msg)
			return
		}

		logger.Notice("received new password")

		if err := s.db.setPassword(user, password1); err != nil {
			s.handleError(w, &dualError{err: err, errPub: http.StatusInternalServerError}, "")
			return
		}

		values := map[string]interface{}{
			"student": user,
		}

		if err := serve(w, r, "password2.html", values); err != nil {
			s.handleError(w, err, "")
			return
		}
	}
}

func (s *server) handleReferenceUnauth() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if err := serve(w, r, "references.html", nil); err != nil {
			s.handleError(w, err, "")
		}
	}
}

func (s *server) handleReferenceAuth() HandlerFuncAuth {
	return func(w http.ResponseWriter, r *http.Request, user string) {
		values := map[string]interface{}{
			"student": user,
		}

		if err := serve(w, r, "references.html", values); err != nil {
			s.handleError(w, err, "")
		}
	}
}

func (s *server) handleLogout() HandlerFuncAuth {
	return func(w http.ResponseWriter, r *http.Request, user string) {
		serveLogin(w)
	}
}

func serveLogin(w http.ResponseWriter) {
	w.Header().Set("WWW-Authenticate", `Basic realm="Restricted"`)
	http.Error(w, "unauthorized", http.StatusUnauthorized)
	logger.Notice("not authenticated: requesting credentials")
}

func serve(w http.ResponseWriter, req *http.Request, file string, values map[string]interface{}) (dualErr *dualError) {
	funcMap := template.FuncMap{
		"normalizeUsername": normalizeUsername,
	}

	p := path.Join(*root, file)

	tmpl, err := template.New(path.Base(p)).Funcs(funcMap).ParseFiles(p)
	if err != nil {
		if os.IsNotExist(err) {
			return &dualError{err: err, errPub: http.StatusNotFound}
		} else {
			return &dualError{err: err, errPub: http.StatusInternalServerError}
		}
	}

	w.Header().Set("Content-Type", "text/html")

	if req.Host == "" {
		msg := "no host value in request"
		return &dualError{err: errors.New(msg), errPub: http.StatusBadRequest}
	}

	err = tmpl.Execute(w, values)
	if err != nil {
		return &dualError{err: err, errPub: http.StatusInternalServerError}
	}

	return nil
}
