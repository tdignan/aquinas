package main

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"strings"
	"testing"
	"time"
)

func TestHash(t *testing.T) {
	/* NOTE: Go hash must match sh hash (i.e., sha256sum). */

	s := "Hello, world!"

	stdin := strings.NewReader(s)
	output, _, err := run(stdin, nil, "sha256sum")
	if err != nil {
		t.Error(err.Error())
	}

	h1 := strings.Split(string(output), " ")[0]
	h2 := hashCalc(s)

	if !hashSame(h1, h2) {
		t.Error(strings.ToUpper(h1) + " != " + strings.ToUpper(h2))
	}
}

func TestHashBad(t *testing.T) {
	s1 := "Hello, world!"
	s2 := "Goodbye, world!"

	stdin := strings.NewReader(s1)
	output, _, err := run(stdin, nil, "sha256sum")
	if err != nil {
		t.Error(err.Error())
	}

	h1 := strings.Split(string(output), " ")[0]
	h2 := hashCalc(s2)

	if hashSame(h1, h2) {
		t.Error(strings.ToUpper(h1) + " == " + strings.ToUpper(h2))
	}
}

func TestDecodeProject(t *testing.T) {
	var proj project

	data := `{ "name": "test",
	           "languages": [ "C", "Go" ],
	           "prerequisites": [ "parent" ],
	           "forbidden": "goto",
	           "compiler": "C",
	           "altmain": { "C": true },
	           "checks": [{ "command": "true",
	                        "stdin": null,
	                        "stdout": null,
	                        "stderr": null,
	                        "exitCode": 0
	                     }]
	         }`

	reader := strings.NewReader(data)
	proj, err := decodeProject(reader)
	if err != nil {
		t.Error(err.Error())
	}

	if proj.Name != "test" {
		t.Error("did not properly decode name from " + data)
	}

	if len(proj.Languages) != 2 || proj.Languages[0] != "C" ||
	                               proj.Languages[1] != "Go" {
		t.Error("did not properly decode languages from " + data)
	}

	if len(proj.Prerequisites) != 1 || proj.Prerequisites[0] != "parent" {
		t.Error("did not properly decode prerequisites from " + data)
	}

	if proj.Forbidden != "goto" {
		t.Error("did not properly decode forbidden from " + data)
	}

	if val, found := proj.Altmain["C"]; !found || val != true {
		t.Error("did not properly decode altmain from " + data)
	}

	if proj.Checks[0].Command != "true" {
		t.Error("did not properly decode command from " + data)
	}
}

func TestRunStdout(t *testing.T) {
	output, _, err := run(nil, nil, "echo", "foo")
	if err != nil {
		t.Error(err.Error())
	}
	if string(output) != "foo\n" {
		t.Error("unexpected value in output: " + string(output))
	}
}

func TestRunStdin(t *testing.T) {
	stdin := strings.NewReader("foo\n")
	output, _, err := run(stdin, nil, "cat")
	if err != nil {
		t.Error(err.Error())
	}
	if string(output) != "foo\n" {
		t.Error("unexpected value in output: " + string(output))
	}
}

func TestRunStderr(t *testing.T) {
	_, _, err := run(nil, nil, "ls", "/file-does-not-exist")
	if err == nil {
		t.Error("expected error but did not receive one")
	}
	if "ls: cannot access '/file-does-not-exist': No such file or directory\n" != err.Error() {
		t.Error("unexpected value in stderr: " + err.Error())
	}
}

func TestRunStderrExitZero(t *testing.T) {
	_, emsg, err := run(nil, nil, "sh", "-c", "echo foo >&2")
	if err != nil {
		t.Error(err.Error())
	}
	if string(emsg) != "foo\n" {
		t.Error("unexpected value in stderr: " + string(emsg))
	}
}

func TestRunExitCode(t *testing.T) {
	_, _, err := run(nil, nil, "false")
	if err == nil {
		t.Error("expected error but did not receive one")
	}
	if stderrErr, ok := err.(*stderrError); ok {
		if stderrErr.exitCode != 1 {
			t.Error("received wrong exit code")
		}
	} else {
		t.Error(err.Error())
	}
}

func TestRunBadCommand(t *testing.T) {
	_, _, err := run(nil, nil, "command-does-not-exist")
	if err == nil {
		t.Error("non-existing command did not produce error")
	}
}

func TestRunTimedStdout(t *testing.T) {
	output, _, err := runTimed(1 * time.Second, nil, nil, "echo", "foo")
	if err != nil {
		t.Error(err.Error())
	}
	if string(output) != "foo\n" {
		t.Error("unexpected value in output: " + string(output))
	}
}

func TestRunTimedStdin(t *testing.T) {
	stdin := strings.NewReader("foo\n")
	output, _, err := runTimed(1 * time.Second, stdin, nil, "cat")
	if err != nil {
		t.Error(err.Error())
	}
	if string(output) != "foo\n" {
		t.Error("unexpected value in output: " + string(output))
	}
}

func TestRunTimedStderr(t *testing.T) {
	_, _, err := runTimed(1 * time.Second, nil, nil, "ls", "/file-does-not-exist")
	if err == nil {
		t.Error("expected error but did not receive one")
	}
	if "ls: cannot access '/file-does-not-exist': No such file or directory\n" != err.Error() {
		t.Error("unexpected value in stderr: " + err.Error())
	}
}

func TestRunTimedStderrExitZero(t *testing.T) {
	_, emsg, err := runTimed(1 * time.Second, nil, nil, "sh", "-c", "echo foo >&2")
	if err != nil {
		t.Error(err.Error())
	}
	if string(emsg) != "foo\n" {
		t.Error("unexpected value in stderr: " + string(emsg))
	}
}

func TestRunTimedExitCode(t *testing.T) {
	_, _, err := runTimed(1 * time.Second, nil, nil, "false")
	if err == nil {
		t.Error("expected error but did not receive one")
	}
	if stderrErr, ok := err.(*stderrError); ok {
		if stderrErr.exitCode != 1 {
			t.Error("received wrong exit code")
		}
	} else {
		t.Error(err.Error())
	}
}

func TestRunTimedBadCommand(t *testing.T) {
	_, _, err := runTimed(1 * time.Second, nil, nil, "command-does-not-exist")
	if err == nil {
		t.Error("non-existing command did not produce error")
	}
}

func TestRunTimedOut(t *testing.T) {
	_, _, err := runTimed(1 * time.Second, nil, nil, "sleep", "2")
	switch err {
	case ErrTimeout:
		return
	case nil:
		t.Error("process did not return error; should have timed out")
	default:
		t.Error("should have timed out, but received:" + err.Error())
	}
}

func TestRunRemoteStdout(t *testing.T) {
	output, _, err := runRemote(nil, nil, "root@aquinas-git." + conf.Domain, "echo", "foo")
	if err != nil {
		t.Error(err.Error())
	}
	if string(output) != "foo\n" {
		t.Error("unexpected value in stderr: " + string(output))
	}
}

func TestRunRemoteStderr(t *testing.T) {
	_, _, err := runRemote(nil, nil, "root@aquinas-git." + conf.Domain, "ls", "/file-does-not-exist")
	if err == nil {
		t.Error("expected error but did not receive one")
	}
	if "ls: /file-does-not-exist: No such file or directory\n" != err.Error() {
		t.Error("unexpected value in stderr: " + err.Error())
	}
}

func TestRunRemoteFail(t *testing.T) {
	_, _, err := runRemote(nil, nil, "root@aquinas-git." + conf.Domain, "false")
	if err == nil {
		t.Error("expected error but did not receive one")
	}
	if stderrErr, ok := err.(*stderrError); ok {
		if stderrErr.exitCode != 1 {
			t.Error("received wrong exit code")
		}
	} else {
		t.Error(err.Error())
	}
}

func TestRunSshStdout(t *testing.T) {
	stdin := strings.NewReader("echo foo\n")
	output, _, err := runSsh(stdin, nil, "root@aquinas-git." + conf.Domain)
	if err != nil {
		t.Error(err.Error())
	}
	if string(output) != "foo\n" {
		t.Error("unexpected value in stderr: " + string(output))
	}
}

func TestRunSshStderr(t *testing.T) {
	stdin := strings.NewReader("ls /file-does-not-exist")
	_, _, err := runRemote(stdin, nil, "root@aquinas-git." + conf.Domain)
	if err == nil {
		t.Error("expected error but did not receive one")
	}
	if "ls: /file-does-not-exist: No such file or directory\n" != err.Error() {
		t.Error("unexpected value in stderr: " + err.Error())
	}
}

func TestRunSshFail(t *testing.T) {
	stdin := strings.NewReader("false")
	_, _, err := runSsh(stdin, nil, "root@aquinas-git." + conf.Domain)
	if err == nil {
		t.Error("expected error but did not receive one")
	}
	if stderrErr, ok := err.(*stderrError); ok {
		if stderrErr.exitCode != 1 {
			t.Error("received wrong exit code")
		}
	} else {
		t.Error(err.Error())
	}
}

func TestKahn(t *testing.T) {
	graph := []Node {
		{ Name: "four", Prereq: map[string]bool{ "three": true }},
		{ Name: "one" },
		{ Name: "three", Prereq: map[string]bool{ "two": true }},
		{ Name: "two", Prereq: map[string]bool{ "one": true }},
	}

	sorted1      := []string{ "one", "two", "three", "four" }
	sorted2, err := kahn(graph)

	if err != nil {
		t.Error(err.Error())
	}

	if len(sorted1) != len(sorted2) {
		t.Error("sort failed:" + fmt.Sprintf("%v", sorted2))
	}

	for i, v := range sorted2 {
		if v != sorted1[i] {
			t.Error("sort failed: " + fmt.Sprintf("%v", sorted2))
		}
	}
}

func TestKahnPartial(t *testing.T) {
	graph := []Node {
		{ Name: "four", Prereq: map[string]bool{ "one": true }},
		{ Name: "three" },
		{ Name: "one" },
		{ Name: "two" },
	}

	sorted, err := kahn(graph)

	if err != nil {
		t.Error(err.Error())
	}

	oneSeen := false
	for _, v := range sorted {
		switch(v) {
		case "four":
			if !oneSeen {
				t.Error("sort failed: four before one")
			}
		case "one":
			oneSeen = true
		}
	}
}

func TestKahnCycle(t *testing.T) {
	graph := []Node {
		{ Name: "one", Prereq: map[string]bool{ "two": true }},
		{ Name: "two", Prereq: map[string]bool{ "one": true }},
	}

	_, err := kahn(graph)
	if err == nil {
		t.Error("expected error but did not receive one")
	}
}

func TestSafe(t *testing.T) {
	s1 := "abcdefghijklmnopqrstuvwxyz" +
	      "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
	      "0123456789" +
	      "-@.+/ "
	s2, err := safe(func(string) string { return s1 }, "ignored")
	if err != nil {
		t.Error(err.Error())
	}
	if s1 != s2 {
		t.Error("safe modified " + s1)
	}
}

func TestSafeDangerous(t *testing.T) {
	s1 := ";"
	s2, err := safe(func(string) string { return s1 }, "ignored")
	if err == nil {
		t.Error("expected error but did not receive one")
	}
	if "" != s2 {
		t.Error("safe did not produce \"\" for dangerous input")
	}
}

func TestSafeDangerousBeginning(t *testing.T) {
	s1 := ";a"
	s2, err := safe(func(string) string { return s1 }, "ignored")
	if err == nil {
		t.Error("expected error but did not receive one")
	}
	if "" != s2 {
		t.Error("safe did not produce \"\" for dangerous input")
	}
}

func TestSafeDangerousMiddle(t *testing.T) {
	s1 := "a;a"
	s2, err := safe(func(string) string { return s1 }, "ignored")
	if err == nil {
		t.Error("expected error but did not receive one")
	}
	if "" != s2 {
		t.Error("safe did not produce \"\" for dangerous input")
	}
}

func TestSafeDangerousEnd(t *testing.T) {
	s1 := "a;"
	s2, err := safe(func(string) string { return s1 }, "ignored")
	if err == nil {
		t.Error("expected error but did not receive one")
	}
	if "" != s2 {
		t.Error("safe did not produce \"\" for dangerous input")
	}
}

func TestSafeKey(t *testing.T) {
	s1 := "ssh-rsa SGVsbG8sIHdvcmxkIQo= user@example.com"
	s2, err := safeKey(func(string) string { return s1 }, "ignored")
	if err != nil {
		t.Error(err.Error())
	}
	if s1 != s2 {
		t.Error("safeKey modified " + s1)
	}
}

func TestSafeKeySpaces(t *testing.T) {
	s1 := "  ssh-rsa  SGVsbG8sIHdvcmxkIQo=  user@example.com  "
	_, err := safeKey(func(string) string { return s1 }, "ignored")
	if err != nil {
		t.Error(err.Error())
	}
}

func TestSafeKeyNewlines(t *testing.T) {
	s1 := "\n\nssh-rsa SGVsbG8sIHdvcmxkIQo= user@example.com\n\n"
	_, err := safeKey(func(string) string { return s1 }, "ignored")
	if err != nil {
		t.Error(err.Error())
	}
}

func TestSafeEmail(t *testing.T) {
	s1 := "user@example.com"
	s2, err := safeEmail(func(string) string { return s1 }, "ignored")
	if err != nil {
		t.Error(err.Error())
	}
	if s1 != s2 {
		t.Error("safeEmail modified " + s1)
	}
}

func TestSafeEmail2(t *testing.T) {
	s1 := "User <user@example.com>"
	s2, err := safeEmail(func(string) string { return s1 }, "ignored")
	if err != nil {
		t.Error(err.Error())
	}
	if s2 != "user@example.com" {
		t.Error("safeEmail mis-parsed " + s1)
	}
}

func TestSafeEmailIllegalSpaceLocalPart(t *testing.T) {
	s1 := "User <user @example.com>"
	_, err := safeEmail(func(string) string { return s1 }, "ignored")
	if err == nil {
		t.Error("safeEmail accepted space in local part")
	}
}

func TestSafeEmailIllegalSpaceDomain(t *testing.T) {
	s1 := "User <user@example .com>"
	_, err := safeEmail(func(string) string { return s1 }, "ignored")
	if err == nil {
		t.Error("safeEmail accepted space in domain")
	}
}

func TestAllowedEmailDomain(t *testing.T) {
	if !allowedEmailDomain(conf.EmailSender) {
		t.Error("did not permit " + conf.EmailSender)
	}
}

func TestAllowedEmailDomainDeny(t *testing.T) {
	email := "user@example.invalid" /* RFC 2606. */
	if allowedEmailDomain(email) {
		t.Error("permitted " + email)
	}
}

func TestNormalizeUsername(t *testing.T) {
	if normalizeUsername(conf.EmailSender) != "mike_at_flyn_dot_org" {
		t.Error("failed to normalize " + conf.EmailSender)
	}
}

func TestNormalizeUsernameNotLong(t *testing.T) {
	email     := "01234567890123456789012345678901"
	if normalizeUsername(email) != email {
		t.Error("failed to normalize " + email)
	}
}

func TestNormalizeUsernameLong(t *testing.T) {
	email     := "012345678901234567890123456789012"
	sum       := md5.Sum([]byte(email))
	hashEmail := hex.EncodeToString(sum[:])
	hashEmail  = "u" + hashEmail[1:]
	if normalizeUsername(email) != hashEmail {
		t.Error("failed to normalize " + email)
	}
}

func TestNormalizeUsernameCompatible(t *testing.T) {
	cmdline := ". aquinas-functions; calcnorm " + conf.EmailSender
	username, _, err := run(nil, nil, "bash", "-c", cmdline)
	if err != nil {
		t.Error("failed to run calcnorm " + conf.EmailSender + ": " + err.Error())
	}

	if normalizeUsername(conf.EmailSender) != string(username)[:len(username) - 1] {
		t.Error("normalize: " + normalizeUsername(conf.EmailSender) +
                        " != " + string(username))
	}
}
