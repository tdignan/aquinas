package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path"
)

const CONFIG      = "aquinas-build.json"
const DESCRIPTION = "description.json"

type config struct {
	Projects string `json:projects`
}

func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

func main() {
	var config config
	var nodes []Node
	projects := make(map[string]project)

	f, err := os.Open(CONFIG)
	if err != nil {
		log.Fatal(err)
	}

	dec := json.NewDecoder(f)

	if err := dec.Decode(&config); err != nil {
		log.Fatal(err)
	}

	files, err := ioutil.ReadDir(config.Projects)
	if err != nil {
		log.Fatal(err)
	}

	for _, file := range files {
		if file.Name()[0] == '.' ||
		   file.Name() == "references.bib" ||
		   file.Name() == "Makefile" {
			continue
		}

		var project project
		prerequisites := make(map[string]bool)

		f, err := os.Open(path.Join(config.Projects, file.Name(), DESCRIPTION))
		if err != nil {
			log.Fatal(err)
		}

		dec := json.NewDecoder(f)

		if err := dec.Decode(&project); err != nil {
			log.Fatal(err)
		}

		for _, v := range project.Prerequisites {
			prerequisites[v] = true
		}

		nodes = append(nodes, Node{ Name: project.Name, Prereq: prerequisites })
		projects[project.Name] = project
	}

	sorted, err := kahn(nodes)
	if err != nil {
		log.Fatal(err)
	}

	for _, name := range sorted {
		for _, lang := range projects[name].Languages {
			if lang == "none" {
				fmt.Printf("<li class=\"{{if .result}} {{- index .result \"%s\"}} {{- end}}\"><a href=\"%s.html\">%s</a>: %s</li>\n", name, name, name, projects[name].Summary)
			} else {
				fmt.Printf("<li class=\"{{if .result}} {{- index .result \"%s%s\"}} {{- end}} project%s\"><a class=\"link%s\" href=\"%s%s.html\">%s in %s</a>: %s</li>\n", name, lang, lang, lang, name, lang, name, lang, projects[name].Summary)
			}
		}
	}
}
