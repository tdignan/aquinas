package main

/* Run as user http on aquinas-www. */

/*
 * Thank you to Mat Ryer for his tips at:
 * https://medium.com/statuscode/how-i-write-go-http-services-after-seven-years-37c208122831
 */

import (
	"flag"
	"log/syslog"
	"net/http"
	"os"
	"os/user"
	"strconv"
)

var root  = flag.String("root", "/www", "root directory for served documents")
var dummy = flag.Bool  ("dummy", false, "execute using dummy database")
var secret  string
var logger *syslog.Writer

var errorMap = map[int]string {
	http.StatusInternalServerError: "unexpected error; contact admin",
	http.StatusNotFound: "not found",
	http.StatusBadRequest: "bad request",
}

/* One error message for log, another for httpd client. */
type dualError struct {
	err error
	errPub int
}

func (e *dualError) Error() string {
	return e.err.Error()
}

func (e *dualError) errorPub() int {
	return e.errPub
}

type server struct {
	db       db
	handler *http.ServeMux
}

type HandlerFuncAuth func(w http.ResponseWriter, r *http.Request, user string)

func main() {
	var err error
	var s server

	flag.Parse()

	logger, err = syslog.New(syslog.LOG_NOTICE, os.Args[0])
	if err != nil {
		panic(err)
	}

	user, err := user.LookupId(strconv.Itoa(os.Geteuid()))
	if err != nil {
		fail(logger.Err, err.Error())
	}

	/* Required so SSH can find its keys. */
	if err := os.Setenv("HOME", user.HomeDir); err != nil {
		fail(logger.Err, err.Error())
	}

	secret, err = uuid()
	if err != nil {
		fail(logger.Err, err.Error())
	}

	if *dummy {
		s.db = make(dbDummy)
	} else {
		s.db = make(dbFilesystem)
	}

	s.handler = http.NewServeMux()
	s.routes()

	/*
	 * TODO: Support TLS; I am presently using lighttpd's mod-proxy to provide TLS.
	 * err := http.ListenAndServeTLS(":https", "server.cert", "server.key", nil)
	 */
	if err = http.ListenAndServe(":http", s.handler); err != nil {
		logger.Err("error starting: " + err.Error())
	}

	os.Exit(1)
}
