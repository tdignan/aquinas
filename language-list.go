package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path"
	"sort"
)

const CONFIG      = "aquinas-build.json"
const DESCRIPTION = "description.json"

type config struct {
	Projects string `json:projects`
}

/* Return Javascript list representing all of s1 except s, "none", and "All." */
func others(s1 []string, str string) (list string) {
	list = "["
	for _, s := range(s1) {
		switch s {
		case str, "none", "All":
			continue
		}
		list += "'" + s + "',"
	}
	list += "]"
	return
}

func main() {
	var config config
	languages := make(map[string]bool)

	f, err := os.Open(CONFIG)
	if err != nil {
		log.Fatal(err)
	}

	dec := json.NewDecoder(f)

	if err := dec.Decode(&config); err != nil {
		log.Fatal(err)
	}

	files, err := ioutil.ReadDir(config.Projects)
	if err != nil {
		log.Fatal(err)
	}

	for _, file := range files {
		if file.Name()[0] == '.' ||
		   file.Name() == "references.bib" ||
		   file.Name() == "Makefile" {
			continue
		}

		var project project

		f, err := os.Open(path.Join(config.Projects, file.Name(), DESCRIPTION))
		if err != nil {
			log.Fatal(err)
		}

		dec := json.NewDecoder(f)

		if err := dec.Decode(&project); err != nil {
			log.Fatal(err)
		}

		for _, v := range project.Languages {
			languages[v] = true
		}
	}

	var keys []string
	for k := range languages {
		keys = append(keys, k)
	}

	sort.Strings(keys)
	keys = append([]string{"All"}, keys...)

	for _, k := range keys {
		switch k {
		case "none":
			continue
		case "All":
			fmt.Printf(`
<input type="radio" name="language" checked onclick="var others = %s;

for (var i = 0; i < others.length; i++) {
	var items = document.getElementsByClassName('project' + others[i]);

	for (var j = 0; j < items.length; j++) {
		items[j].style.color = 'black';
	}

	var items = document.getElementsByClassName('link' + others[i]);

	for (var j = 0; j < items.length; j++) {
		items[j].style.color = '#28c';
	}
}">%s`, others(keys, ""), k)
		default:
			fmt.Printf(`
<input type="radio" name="language" onclick="var others = %s;

for (var i = 0; i < others.length; i++) {
	var items = document.getElementsByClassName('project' + others[i]);

	for (var j = 0; j < items.length; j++) {
		items[j].style.color = 'lightgray';
	}

	items = document.getElementsByClassName('link' + others[i]);

	for (var j = 0; j < items.length; j++) {
		items[j].style.pointerEvents = 'none';
		items[j].style.cursor = 'default';
		items[j].style.color = 'lightgray';
	}

	items = document.getElementsByClassName('project%s');

	for (var j = 0; j < items.length; j++) {
		items[j].style.color = 'black';
	}

	items = document.getElementsByClassName('link%s');

	for (var j = 0; j < items.length; j++) {
		items[j].style.pointerEvents = 'auto';
		items[j].style.cursor = 'auto';
		items[j].style.color = '#28c';
	}
}">%s`, others(keys, k), k, k, k)
		}
	}
}
