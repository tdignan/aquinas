package main

import (
	"fmt"
	"os"
)

func main() {
	exitcode := 0

	stdout, stderr, err := enqueue(os.Args[1:]...)
	if err != nil {
		fmt.Fprintf(os.Stderr, os.Args[0] + ": " + err.Error())
		exitcode = 1
	}

	fmt.Fprintf(os.Stdout, "%s", stdout)
	fmt.Fprintf(os.Stderr, "%s", stderr)

	os.Exit(exitcode)
}
