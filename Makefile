domain        = $(shell jq -r .domain aquinas.json)
projects-repo = $(shell jq -r .projects aquinas-build.json)
webhost       = aquinas-www.$(domain)
webadmin      = teacher

# C99, because otherwise cpp defines "linux" as "1."
cpp = cpp -traditional-cpp -C -P -nostdinc -std=c99 -Werror -Iwww

hosts= \
	aquinas-git \
	aquinas-www \
	aquinas-target \
	aquinas-user \

# Build with ext4 security labels for use with libcap-bin.
# To generate a patch such as this one, enter the OpenWrt tree and
# (1) run "make kernel_menuconfig" and (2) run "git diff target/linux."
aquinas-www_openwrt_build_opts=-k openwrt-build/definitions/aquinas-www.kernel-config-patch

openwrt-build=~/Source/openwrt-build/openwrt-build
files-common=~/Source/openwrt-build-$(domain)/files/common

_htmls = \
	www/landing.html \
	www/register.html \
	www/register2.html \
	www/register3.html \
	www/reset.html \
	www/reset2.html \
	www/reset3.html \
	www/ssh.html \
	www/ssh2.html \
	www/password.html \
	www/password2.html

emails = \
	www/email-register \
	www/email-reset

extension_AMD64 = .S
extension_C = .c
extension_Go = .go

langsuffix = $(if $(findstring none,$(1)),,$(1))

css = \
	www/css/aquinas.css \
	www/css/LaTeXML.css \
	www/css/ltx-amsart.css \
	www/css/ltx-article.css \
	www/css/ltx-listings.css

all: list htmls programs installs conf # check

.PHONY: check
check:
	for i in $(htmls) $(_htmls); do \
		tidy -utf8 -m -q -errors $$i || exit 1; \
	done
	linkchecker --threads=-1 --no-warnings $(htmls) $(_htmls)

# macOS ls does not support -I.
define projects
$(shell ls $(projects-repo) | grep -v "Makefile\|references.bib")
endef

define languages
$(shell jq -r .languages[] $(projects-repo)/$(1)/description.json)
endef

define summary
$(shell jq -r .summary $(projects-repo)/$(1)/description.json)
endef

# No getent on macOS.
ifneq ($(shell which getent),)
define service_ip
$(shell getent ahostsv4 aquinas-target.$(domain) | grep STREAM | cut -d ' ' -f 1)
endef
else
define service_ip
127.0.0.1
endef
endif

define service_program
$(shell jq -r .service_program $(projects-repo)/$(1)/description.json | sed 's/^null$$//g')
endef

define service_port
$(shell jq -r .service_port $(projects-repo)/$(1)/description.json)
endef

www/css/aquinas.css: www/less/style.less
	lessc --js $^ $@

define BUILDRECORD_template
# $(1): project
# $(2): language
cleans += www/$(1)$(2).tex
cleans += www/$(1)$(2).checked
cleans += www/$(1)$(2)-title.tex
cleans += www/$(1)$(2)-summary.tex
cleans += www/$(1)$(2)-project.tex
cleans += www/$(1)$(2)-language.tex
cleans += www/$(1)$(2)-extension.tex
cleans += www/$(1)$(2)-service-ip.tex
cleans += www/$(1)$(2)-service-port.tex
cleans += www/$(1)$(2)-instructions.tex
cleans += www/references.bib
htmls  += www/$(1)$(2).html

# Should also install git-hooks/pre-commit at .git/hooks/pre-commit.
www/$(1)$(2).checked: ./git-hooks/pre-commit $(projects-repo)/$(1)/description.json
	./git-hooks/pre-commit $(1)
	touch www/$(1)$(2).checked

www/$(1)$(2)-title.tex:
	printf '%s' "$(1)" > $$@

www/$(1)$(2)-summary.tex:
	printf '%s' "$(call summary,$(1))" > $$@

# Project name without language suffix; \jobname in LaTeX has suffix.
www/$(1)$(2)-project.tex:
	printf '\\newcommand{\\project}{%s}' "$(1)" > $$@

www/$(1)$(2)-language.tex:
	printf '\\newcommand{\\lang}{%s}' "$(2)" > $$@

www/$(1)$(2)-extension.tex:
	printf '\\newcommand{\\extension}{%s}' "$(extension_$(2))" > $$@

www/$(1)$(2)-service-ip.tex:
	printf '\\newcommand{\\serviceip}{%s\\xspace}' "$(call service_ip)" > $$@

www/$(1)$(2)-service-port.tex: $(projects-repo)/$(p)/description.json
	printf '\\newcommand{\\serviceport}{%s\\xspace}' "$(call service_port,$(1))" > $$@

ifneq ($(call service_program,$(1)),)
ifeq ($(findstring www/$(call service_program,$(1)),$(cleans)),)
cleans += www/$(call service_program,$(1))

www/$(call service_program,$(1)): $(projects-repo)/$(1)/$(call service_program,$(1))
	./aquinas-sanitize $$< >$$@
endif
endif

ifneq ($(wildcard $(projects-repo)/$(1)/instructions.md),)
www/$(1)$(2)-instructions.tex: $(projects-repo)/$(1)/instructions.md $(if $(call service_program,$(1)),www/$(call service_program,$(1)),)
	if [ -f $(projects-repo)/references.bib ]; then \
		cp $(projects-repo)/references.bib www/; \
	fi
	pandoc -f markdown_strict -t latex $(projects-repo)/$(1)/instructions.md >$$@
else
www/$(1)$(2)-instructions.tex: $(projects-repo)/$(1)/instructions.tex $(if $(call service_program,$(1)),www/$(call service_program,$(1)),)
	if [ -f $(projects-repo)/references.bib ]; then \
		cp $(projects-repo)/references.bib www/; \
	fi
	cp $(projects-repo)/$(1)/instructions.tex $$@
endif

www/$(1)$(2).tex: www/$(1)$(2).checked \
                  www/$(1)$(2)-title.tex \
                  www/$(1)$(2)-summary.tex \
                  www/$(1)$(2)-project.tex \
                  www/$(1)$(2)-language.tex \
                  www/$(1)$(2)-extension.tex \
                  www/$(1)$(2)-service-ip.tex \
                  www/$(1)$(2)-service-port.tex \
                  www/$(1)$(2)-instructions.tex
	cp www/template-record.tex $$@
endef

$(foreach p,$(call projects), \
	$(foreach l,$(call languages,$(p)), \
		$(eval $(call BUILDRECORD_template,$(p),$(call langsuffix,$(l)))) \
	) \
)

define BUILDPROJLIST_template
cleans  += www/projects.html
cleans  += www/language-list.html
cleans  += www/project-list.html
_htmls  += www/projects.html

www/language-list.html: language-list
	./language-list >$$@

www/project-list.html: project-list $(foreach p,$(call projects),$(projects-repo)/$(p)/description.json)
	./project-list >$$@

www/projects.html: www/projects.html.in www/head.html www/menu.html www/foot.html www/project-list.html www/language-list.html
	$(cpp) $$< >$$@
endef

$(eval $(call BUILDPROJLIST_template))

define BUILDREF_template
cleans += www/reference-cmd-list.tex
cleans += www/reference-c-list.tex
cleans += www/reference-go-list.tex
cleans += www/reference-python-list.tex
cleans += www/reference-fn-c-list.tex
cleans += www/reference-fn-go-list.tex
cleans += www/reference-fn-python-list.tex
cleans += www/references.tex
htmls  += www/references.html

www/reference-cmd-list.tex: $(htmls)
	grep -h cmddesc $(patsubst %,$(projects-repo)/%/instructions.tex,$(call projects)) | sort | uniq >$$@

www/reference-c-list.tex: $(htmls)
	grep -h elemcdesc $(patsubst %,$(projects-repo)/%/instructions.tex,$(call projects)) | sort | uniq >$$@

www/reference-go-list.tex: $(htmls)
	grep -h elemgodesc $(patsubst %,$(projects-repo)/%/instructions.tex,$(call projects)) | sort | uniq >$$@

www/reference-python-list.tex: $(htmls)
	grep -h elempythondesc $(patsubst %,$(projects-repo)/%/instructions.tex,$(call projects)) | sort | uniq >$$@

www/reference-fn-c-list.tex: $(htmls)
	grep -h fncdesc $(patsubst %,$(projects-repo)/%/instructions.tex,$(call projects)) | sort | uniq >$$@

www/reference-fn-go-list.tex: $(htmls)
	grep -h fngodesc $(patsubst %,$(projects-repo)/%/instructions.tex,$(call projects)) | sort | uniq >$$@

www/reference-fn-python-list.tex: $(htmls)
	grep -h fnpythondesc $(patsubst %,$(projects-repo)/%/instructions.tex,$(call projects)) | sort | uniq >$$@

www/references.tex: www/reference-cmd-list.tex \
                    www/reference-c-list.tex \
                    www/reference-go-list.tex \
                    www/reference-python-list.tex \
                    www/reference-fn-c-list.tex \
                    www/reference-fn-go-list.tex \
                    www/reference-fn-python-list.tex
	cp www/template-references.tex $$@
endef

$(eval $(call BUILDREF_template))

define BUILDTEXPAGE_template
cleans += $(1)
cleans += $(css)
cleans += $(patsubst %.html,%.xml,$(1))

$(1): $(patsubst %.html,%.tex,$(1)) www/head.html www/menu.html www/foot.html www/css/aquinas.css
	latexml   --destination=$(patsubst %.html,%.xml,$(1)) $(patsubst %.html,%.tex,$(1))
	latexmlpost --destination=$(1) --format=html5 --stylesheet=www/LaTeXML-html5.xsl --css css/aquinas.css $(patsubst %.html,%.xml,$(1))
	mv www/*.css www/css/
	sed -i 's/href="\([^c][^s][^s].*\)\.css"/href="css\/\1.css"/g' $(1)
endef

$(foreach p,$(htmls),$(eval $(call BUILDTEXPAGE_template,$(p))))

define BUILDPAGE_template
cleans += www/$(1)
cleans += $(css)

www/$(1): $(patsubst %.html,%.html.in,www/$(1)) www/head.html www/menu.html www/foot.html www/css/aquinas.css
	$(cpp) $$< >$$@
endef

$(eval $(call BUILDPAGE_template,landing.html))
$(eval $(call BUILDPAGE_template,password2.html))
$(eval $(call BUILDPAGE_template,password.html))
$(eval $(call BUILDPAGE_template,ssh2.html))
$(eval $(call BUILDPAGE_template,ssh.html))

list: $(list)

htmls: $(htmls)

define CLEANS_template
cleans += vm-$(1).cfg
cleans += $(1)-openwrt-x86-64-combined-ext4.img
cleans += $(if $(shell grep extra_disks openwrt-build/definitions/$(1).json),$(1)-data-*.img,)
endef

define BUILDVM_template
$(eval $(call CLEANS_template,$(1)))
realcleans += openwrt-$(1)
$(1).diffconfig = $(if $(wildcard openwrt-build/definitions/$(1).diffconfig),openwrt-build/definitions/$(1).diffconfig)

$(1)-openwrt-x86-64-combined-ext4.img: openwrt-build/definitions/$(1).json buildrunsh
	$(openwrt-build) -c openwrt-build/definitions/$(1).json \
	                 -p openwrt-build/definitions/$(1).post \
	                 $$(if $$($(1).diffconfig),-d $$($(1).diffconfig)) \
	                 $(2) $(files-common) openwrt-build/files/$(1)
endef

define BUILDGO_template
programs += $(1)
cleans   += $(1)
cleans   += openwrt-build/files/$(3)/usr/$(4)/$(1)

# NOTE: CGO_ENABLED=0 results in a statically-linked program even when
# using network facilities.
$(1): $(1).go $(2)
	CGO_ENABLED=0 go build $$^

ifneq ($(3),)
installs += $(3)-$(1)

.PHONY: $(3)-$(1)
$(3)-$(1): $(1)
	install -D $(1) openwrt-build/files/$(3)/usr/$(4)/$(1)
endif
endef

define BUILDC_template
programs += $(1)
cleans   += $(1)
cleans   += openwrt-build/files/$(3)/usr/$(4)/$(1)
$(1): $(1).c $(2)
	gcc -static -m64 -o $(1) $$^

ifneq ($(3),)
installs += $(3)-$(1)

.PHONY: $(3)-$(1)
$(3)-$(1): $(1)
	install -D $(1) openwrt-build/files/$(3)/usr/$(4)/$(1)
endif
endef

define BUILDSH_template
installs += $(3)-$(1)
cleans   += openwrt-build/files/$(2)/usr/$(3)/$(1)

.PHONY: $(3)-$(1)
$(3)-$(1): $(1)
	install -D $(1) openwrt-build/files/$(2)/usr/$(3)/$(1)
endef

define BUILDCONF_template
conf   += $(1)
cleans += $(foreach h,$(hosts),openwrt-build/files/$(h)/etc/aquinas/$(1))

.PHONY: $(1)
$(1):
	for h in $(hosts); do \
		install -D $$@ openwrt-build/files/$$$$h/etc/aquinas/$$@; \
	done
endef

define BUILDFW_template
conf   += openwrt-build/files/$(1)/etc/config/firewall.post
cleans += openwrt-build/files/$(1)/etc/config/firewall.post

openwrt-build/files/$(1)/etc/config/firewall.post:
	./aquinas-update-firewall $(projects-repo) $(1) >$$@
endef

$(foreach h,$(hosts),$(eval $(call BUILDVM_template,$(h),$($(h)_openwrt_build_opts))))

$(eval $(call BUILDGO_template,language-list,common.go project.go,,))
$(eval $(call BUILDGO_template,project-list,common.go project.go,,))
$(eval $(call BUILDGO_template,queue,common.go project.go queue-functions.go,aquinas-git,sbin))
$(eval $(call BUILDGO_template,aquinas-enqueue,common.go project.go queue-functions.go,aquinas-git,bin))
$(eval $(call BUILDGO_template,grader,common.go project.go,aquinas-git,sbin))
$(eval $(call BUILDGO_template,httpsh,common.go project.go queue-functions.go,aquinas-git,bin))
$(eval $(call BUILDSH_template,aquinas-deploy-key,aquinas-git,sbin))
$(eval $(call BUILDSH_template,aquinas-initialize-project,aquinas-git,sbin))
$(eval $(call BUILDSH_template,aquinas-initialize-projects,aquinas-git,sbin))
$(eval $(call BUILDSH_template,aquinas-add-student-slave,aquinas-git,sbin))
$(eval $(call BUILDSH_template,aquinas-remove-student-slave,aquinas-git,sbin))
$(eval $(call BUILDSH_template,aquinas-get-ssh-authorized-keys,aquinas-git,sbin))
$(eval $(call BUILDSH_template,aquinas-sanitize,aquinas-git,bin))
$(eval $(call BUILDGO_template,buildrunsh,common.go project.go,aquinas-user,bin))
$(eval $(call BUILDGO_template,httpd,common.go project.go httpd-routes.go httpd-handlers.go httpd-db.go httpd-db-dummy.go httpd-db-filesystem.go,aquinas-www,sbin))
$(eval $(call BUILDSH_template,aquinas-update-www,aquinas-git,sbin))
$(eval $(call BUILDSH_template,aquinas-update-firewall,aquinas-git,sbin))
$(eval $(call BUILDSH_template,aquinas-add-student,aquinas-www,sbin))
$(eval $(call BUILDSH_template,aquinas-remove-student,aquinas-www,sbin))
$(eval $(call BUILDSH_template,aquinas-passwd-student,aquinas-www,sbin))
$(eval $(call BUILDC_template,chrootdrop,,aquinas-target,sbin))
$(eval $(call BUILDFW_template,aquinas-target))
$(eval $(call BUILDFW_template,aquinas-user))
$(eval $(call BUILDCONF_template,aquinas-functions))
$(eval $(call BUILDCONF_template,aquinas.json))

programs: $(programs)

installs: $(installs)

conf: $(conf)

vms: $(foreach h,$(hosts),$(h)-openwrt-x86-64-combined-ext4.img)

.PHONY: publish_htmls
publish_htmls: $(emails) $(htmls) $(_htmls) www/css/ www/fonts/ www/img/ www/js/
	scp -r $^ $(webadmin)@$(webhost):/www/

.PHONY: publish_programs
publish_programs: programs installs conf
	ssh root@aquinas-www.$(domain) /etc/init.d/httpd stop
	ssh root@aquinas-git.$(domain) /etc/init.d/queue stop
	for h in $(hosts); do \
		echo $$h:; \
		scp -rp openwrt-build/files/$$h/* root@$$h.$(domain):/ || exit 1; \
	done
	ssh root@aquinas-www.$(domain) /etc/init.d/httpd start
	ssh root@aquinas-git.$(domain) /etc/init.d/queue start

.PHONY: publish
publish: publish_htmls publish_programs

.PHONY: clean
clean:
	rm -f $(cleans)

.PHONY: realclean
realclean: clean
	rm -rf $(realcleans)
