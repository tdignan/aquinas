package main

type dbDummy map[bool]bool

func (d dbDummy) addStudent(user, password string) (jobUuid string, err error) {
	return uuid()
}

func (d dbDummy) authenticate(user, password string) (err error) {
	return
}

func (d dbDummy) setPassword(user, password string) (err error) {
	return
}

func (d dbDummy) sshAuthKeys(student string) (key string, dualErr *dualError) {
        return "Fake SSH key", nil
}

func (d dbDummy) setSshAuthKeys(student, key string) (err error) {
	return
}

func (d dbDummy) last(student, file string) (last string) {
	return "No error"
}

func (d dbDummy) records(student, file string) (records string) {
	return `<table summary="Records" class="booktabs">
<tr><th class="booktabs">Commit</th><th class="booktabs">Time</th><th class="booktabs">Result</th></tr>
<tr><td>HASH1</td><td>TIME1</td><td>RESULT1</td>
<tr><td>HASH2</td><td>TIME2</td><td>RESULT2</td>
</table>`
}

/* Returns a mapping of project names to "done" or "failed". */
func (d dbDummy) attempts(student string) (result map[string]string, err error) {
	return
}
