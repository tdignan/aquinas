package main

import (
	"crypto/rand"
	"crypto/sha256"
	"crypto/md5"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/mail"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"regexp"
	"strings"
	"syscall"
	"time"
)

type configuration struct {
	Domain string          `json:"domain"`
	Root string            `json:"root"`
	EmailRegex string      `json:"emailRegex"`
	EmailRelay string      `json:"emailRelay"`
	EmailSender string     `json:"emailSender"`
	EmailSenderName string `json:"emailSenderName"`
}

type stderrError struct {
	exitCode int
	stderr []byte
}

type check struct {
	Command   string  `json:"command"`
	Stdin   []byte    `json:"stdin"`
	Stdout  []byte    `json:"stdout"`
	Stderr  []byte    `json:"stderr"`
	ExitCode  int     `json:"exitCode"`
}

type result struct {
	Command   string `json:"command"`
	Stdout  []byte   `json:"stdout"`
	Stderr  []byte   `json:"stderr"`
	ExitCode  int    `json:"exitCode"`
}

type results struct {
	Results []result `json:"results"`
	Hash      string `json:"hash"`
}

type Node struct {
	Name  string
	Prereq map[string]bool /* Set of node names which must precede this. */
}

var ErrTimeout = errors.New("process timed out")
var conf *configuration

func init() {
	if conf == nil {
		f, err := os.Open("aquinas.json")
		if err != nil {
			if f, err = os.Open("/etc/aquinas/aquinas.json"); err != nil {
				panic(err)
			}
		}

		defer f.Close()

		dec   := json.NewDecoder(f)
		conf   = new(configuration)
		if err = dec.Decode(conf); err != nil {
			panic(err)
		}
	}

	/* Ensure regex is well-formed. */
	regexp.MustCompile(conf.EmailRegex)
}

func (e *stderrError) Error() string {
	return string(e.stderr)
}

func fail(fn func (message string) error, message string) {
	fn(message)
	os.Exit(1)
}

func decodeLanguageThenProject(reader io.Reader) (projectInterface, error) {
	var l string
	var err error

	dec := json.NewDecoder(reader)

	if err = dec.Decode(&l); err != nil {
		return nil, err
	}

	switch l {
	case "AMD64":
		var p projectAmd64
		err = dec.Decode(&p)
		return p, err
	case "C":
		var p projectC
		err = dec.Decode(&p)
		return p, err
	case "Go":
		var p projectGo
		err = dec.Decode(&p)
		return p, err
	case "Python":
		var p projectPython
		err = dec.Decode(&p)
		return p, err
	case "":
		var p projectNone
		err = dec.Decode(&p)
		return p, err
	}

	return nil, errors.New("invalid language: " + l)
}

func decodeProject(reader io.Reader) (p project, err error) {
	dec := json.NewDecoder(reader)
	err = dec.Decode(&p)
	return
}

func decodeResults(reader io.Reader) (r results, err error) {
	dec := json.NewDecoder(reader)
	err = dec.Decode(&r)
	return
}

func run(reader io.Reader, env []string, arg0 string, args ...string) (output []byte, emsg []byte, err error) {
	cmd := exec.Command(arg0, args...)
	cmd.Env = append(os.Environ(), env...)

	stdin, err := cmd.StdinPipe()
	if err != nil {
		return nil, nil, err
	}

	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return nil, nil, err
	}

	stderr, err := cmd.StderrPipe()
	if err != nil {
		return nil, nil, err
	}

	if nil != reader {
		go func() {
			defer stdin.Close()
			io.Copy(stdin, reader)
		}()
	}

	if err = cmd.Start(); err != nil {
		return nil, nil, err
	}

	output, err = ioutil.ReadAll(stdout)
	if err != nil {
		return nil, nil, err
	}

	if len(output) == 0 {
		output = nil
	}

	emsg, err = ioutil.ReadAll(stderr)
	if err != nil {
		return nil, nil, err
	}

	if len(emsg) == 0 {
		emsg = nil
	}

	err = cmd.Wait()

	if exitErr, ok := err.(*exec.ExitError); ok {
		if status, ok := exitErr.Sys().(syscall.WaitStatus); ok {
			err = &stderrError{exitCode: status.ExitStatus(), stderr: emsg}
		} else {
			err = errors.New("Failed to extract exit code")
		}
	}

	return
}

func runTimed(duration time.Duration, reader io.Reader, env []string, arg0 string, args ...string) (output []byte, emsg []byte, err error) {
	cmd := exec.Command(arg0, args...)
	cmd.Env = append(os.Environ(), env...)

	stdin, err := cmd.StdinPipe()
	if err != nil {
		return nil, nil, err
	}

	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return nil, nil, err
	}

	stderr, err := cmd.StderrPipe()
	if err != nil {
		return nil, nil, err
	}

	if nil != reader {
		go func() {
			defer stdin.Close()
			io.Copy(stdin, reader)
		}()
	}

	if err = cmd.Start(); err != nil {
		return nil, nil, err
	}

	done := make(chan error, 1)
	go func() {
		output, err = ioutil.ReadAll(stdout)
		if err != nil {
			done <- err
		}

		if len(output) == 0 {
			output = nil
		}

		emsg, err = ioutil.ReadAll(stderr)
		if err != nil {
			done <- err
		}

		if len(emsg) == 0 {
			emsg = nil
		}

		done <- cmd.Wait()
	}()

	select {
	case err = <-done:
		if exitErr, ok := err.(*exec.ExitError); ok {
			if status, ok := exitErr.Sys().(syscall.WaitStatus); ok {
				err = &stderrError{exitCode: status.ExitStatus(), stderr: emsg}
			} else {
				err = errors.New("Failed to extract exit code")
			}
		}
	case <-time.After(duration):
		if err = cmd.Process.Kill(); err == nil {
			err = ErrTimeout
		}
	}

	return
}

func runRemote(reader io.Reader, env []string, host string, args ...string) (output []byte, emsg []byte, err error) {
	prefix := []string{ "-T", host }
	args = append(prefix, args...)
	return run(reader, env, "ssh", args...)
}

func runSsh(reader io.Reader, env []string, host string) (output []byte, emsg []byte, err error) {
	return run(reader, env, "ssh", "-T", host)
}

func execWithStderr(arg0 string, args ...string) (err error) {
	var emsg []byte

	cmd := exec.Command(arg0, args...)

	stderr, err := cmd.StderrPipe()
	if err != nil {
		return
	}

	if err = cmd.Start(); err != nil {
		return
	}

	done := make(chan error, 1)
	go func() {
		emsg, err = ioutil.ReadAll(stderr)
		if err != nil {
			done <- err
			return
		}

		done <- cmd.Wait()
	}()

	select {
	case err = <-done:
		if _, ok := err.(*exec.ExitError); ok {
			err = errors.New(string(emsg))
		}
	case <-time.After(5 * time.Second):
		if err = cmd.Process.Kill(); err == nil {
			err = ErrTimeout
		}
	}

	return err
}

func updateRepo(repo, branch, dir, projName string) (err error) {
	repoDir := path.Join(dir, projName)

	stat, err := os.Stat(repoDir)
	if os.IsNotExist(err) {
		if err = execWithStderr("git", "clone", "-q", repo, "-b", branch, repoDir);
		   err != nil {
			return
		}
	} else if stat.IsDir() {
		if err = execWithStderr("git", "-C", repoDir, "pull");
		   err != nil {
			return
		}
	} else {
		err = errors.New(repoDir + " exists but is not a directory")
	}

	return err
}

func uuid() (uuid string, err error) {
	b := make([]byte, 16)

	if _, err = rand.Read(b); err != nil {
		return
	}

	uuid = fmt.Sprintf("%X-%X-%X-%X-%X", b[0:4], b[4:6], b[6:8], b[8:10], b[10:])

	return
}

func nonce() string {
	n := time.Now().UnixNano()
	return fmt.Sprintf("%X", n)
}

func password() (password string, err error) {
	b := make([]byte, 8)

	if _, err = rand.Read(b); err != err {
		return
	}

	password = fmt.Sprintf("%X", b)

	return
}

func hashCalc(data string) string {
	return fmt.Sprintf("%X", sha256.Sum256([]byte(data)))
}

func hashSame(h1, h2 string) bool {
	return strings.ToUpper(h1) == strings.ToUpper(h2)
}

func normalizeUsername(username string) string {
	username = strings.Replace(username, "@", "_at_", -1)
	username = strings.Replace(username, ".", "_dot_", -1)

	if len(username) > 32 {
		sum := md5.Sum([]byte(username))
		username = hex.EncodeToString(sum[:])
		username = "u" + username[1:]
	}

	return username
}

/* Kahn's algorithm (1962). */
func kahn(nodes []Node) (results []string, err error) {
	/* Set of nodes with no incoming edge. */
	var s[]Node

	/* Gather start nodes with no incoming edges. */
	for _, n := range nodes {
		if len(n.Prereq) == 0 {
			s = append(s, n)
		}
	}

	for len(s) != 0 {
		/* Remove a node from s, and add to results. */
		n := s[0]; s = s[1:]
		results = append(results, n.Name)

		for _, m := range(nodes) {
			if _, ok := m.Prereq[n.Name]; ok {
				delete(m.Prereq, n.Name)
				if len(n.Prereq) == 0 {
					s = append(s, m)
				}
			}
		}
	}

	for _, n := range(nodes) {
		if len(n.Prereq) != 0 {
			err = errors.New("graph has a cycle")
		}
	}

	return
}

func _safe(s1 string) (string, error) {
	blacklist := regexp.MustCompile("[^a-zA-Z0-9@.=+-/ ]")
	if blacklist.Match([]byte(s1)) {
		return "", errors.New("blacklisted rune in string")
	}

	return s1, nil
}

func safe(formValue func(string) string, key string) (value string, err error) {
	value = formValue(key)
	if value == "" {
		return "", errors.New("missing value for form field " + key)
	}

	return _safe(value)
}

func safeKey(formValue func(string) string, s string) (value string, err error) {
	value = strings.TrimSpace(formValue(s))

	blacklist := regexp.MustCompile("[^a-zA-Z0-9@.=+-/ \r\n]")
	if blacklist.Match([]byte(value)) {
		return "", errors.New("blacklisted rune in key " + value)
	}

	keys := strings.Split(value, "\n")
	for n, key := range keys {
		field := strings.Fields(key)
		if len(field) != 3 {
			return "", errors.New(fmt.Sprintf("SSH key %d field count not valid", n))
		}

		if _, err := base64.StdEncoding.DecodeString(field[1]); err != nil {
			return "", errors.New(fmt.Sprintf("SSH key %d base64 not valid", n))
		}
	}

	return
}

func safeEmail(formValue func(string) string, key string) (value string, err error) {
	var address *mail.Address

	raw := formValue(key)

	/* Drop potential '<' and '>' before calling safe(). */
	if address, err = mail.ParseAddress(raw); err != nil {
		return
	}

	if value, err = _safe(address.Address); err != nil {
		return
	}

	return
}

func allowedEmailDomain(email string) bool {
	/* Allow example.com allows for testing; see test-case-battery-07-www-register. */
	allowed := regexp.MustCompile(conf.EmailRegex)
	if allowed.Match([]byte(email)) {
		return true
	}

	return false
}

func gitHost(host string) string {
	if strings.HasPrefix(host, "aquinas-www.") {
		return "aquinas-git." + conf.Domain
	} else {
		return host
	}
}

func langToExt(lang string) string {
	switch lang {
	case "AMD64":
		return ".S"
	case "C":
		return ".c"
	case "Go":
		return ".go"
	default:
		return ""
	}
}

func langToExt2(lang string) string {
	switch lang {
	case "AMD64":
		return ".S"
	case "C":
		return ".c"
	case "Go":
		return ".go"
	case "Python":
		return ".py"
	default:
		return ""
	}
}

func dropExt(s string) string {
	return strings.TrimSuffix(s, filepath.Ext(s))
}
