package main

type db interface {
	addStudent(user, password string) (jobUuid string, err error)
	authenticate(user, password string) (err error)
	setPassword(user, password string) (err error)
	sshAuthKeys(student string) (key string, dualErr *dualError)
	setSshAuthKeys(student, key string) (err error)
	last(student, file string) (last string)
	records(student, file string) (records string)
	attempts(student string) (result map[string]string, err error)
}
