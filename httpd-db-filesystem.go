package main

import (
	"bufio"
	"encoding/base64"
	"errors"
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"strings"
	"time"
)

type file struct {
	contents  string
	timestamp time.Time
}

type record struct {
	sshAuthKeys string
	last        file
	records     file
}

type dbFilesystem map[string]*record

func (d dbFilesystem) addStudent(user, password string) (jobUuid string, err error) {
	stdin := strings.NewReader("add " + user + "\n")
	u, _, err := runSsh(stdin, nil, "aquinas-git." + conf.Domain)
	if err != nil {
		return
	}

	jobUuid = string(u[:len(u)-1]) /* Drop '\n'. */

	if err = d.setPassword(user, password); err != nil {
		return
	}

	return
}

func (d dbFilesystem) passwordHash(user string) (hash string, err error) {
	passwordPath := path.Join("/etc/httpd/accounts", user, "password")

	password, err := ioutil.ReadFile(passwordPath)
	if err != nil {
		return
	}

	hash = string(password[:len(password) - 1]) /* Drop "\n". */
	return
}

func (d dbFilesystem) authenticate(user, password string) (err error) {
	hashedPassword, err := d.passwordHash(user)
	if err != nil {
		err = errors.New("received bad login credentials: " +
		                 "could not read password for " + user)
		return
	}

	if !hashSame(hashedPassword, hashCalc(password)) {
		err = errors.New("received bad login credentials: bad " +
		                 "password for " + user)
		return
	}

	return
}

func (d dbFilesystem) setPassword(user, password string) (err error) {
	userPath := path.Join("/etc/httpd/accounts", user)
	if _, err = os.Stat(userPath); os.IsNotExist(err) {
		if err = os.Mkdir(userPath, 0750); err != nil {
			return
		}
	}

	hash := hashCalc(password)

	passwordPath := path.Join(userPath, "password")
	err = ioutil.WriteFile(passwordPath, []byte(hash + "\n"), 0644)
	return
}

func (d dbFilesystem) sshAuthKeys(student string) (key string, dualErr *dualError) {
	r, found := d[student]
	if !found {
		d[student] = new(record)
	} else if r.sshAuthKeys != "" {
		return r.sshAuthKeys, nil
	}

	stdin := strings.NewReader("ssh " + student + "\n")
	output, _, err := runSsh(stdin, nil, "aquinas-git." + conf.Domain)
	if err != nil {
		return "", &dualError{err: err, errPub: http.StatusInternalServerError}
	}
	key = string(output[:len(output) - 1]) /* Drop "\n". */
	if string(output) == "" {
		logger.Notice("received " + string(output) +
			      " no public SSH key for " + student)
		return
	}

	d[student].sshAuthKeys = key
        return
}

func (d dbFilesystem) setSshAuthKeys(student, key string) (err error) {
	arg := student + ":" + key
	encodedArg := base64.StdEncoding.EncodeToString([]byte(arg))
	stdin := strings.NewReader("key " + encodedArg + "\n")
	_, _, err = runSsh(stdin, nil, "aquinas-git." + conf.Domain)
	if err != nil {
		return
	}

	r, found := d[student]
        if !found {
		r = new(record)
                d[student] = r
        }

	r.sshAuthKeys = key
	return
}

func (d dbFilesystem) last(student, file string) (last string) {
	project := strings.TrimSuffix(file, path.Ext(file)) + "-last"

	p := path.Join(*root, student, project)
	info, err := os.Stat(p)
	if err != nil {
		logger.Notice("could not open " + p + "; assuming no last compile")
		return
	}

	r, found := d[student]
	if !found {
		d[student] = new(record)
	} else if r.last.timestamp.Equal(info.ModTime()) {
		last = r.last.contents
		return
	}

	_last, err := ioutil.ReadFile(p)
	if err != nil {
		logger.Notice("could not open " + p + "; assuming no last compile")
	} else {
		last = string(_last)
		d[student].last.contents  = last
		d[student].last.timestamp = info.ModTime()
	}

	return
}

func (d dbFilesystem) records(student, file string) (records string) {
	project := strings.TrimSuffix(file, path.Ext(file)) + "-records"

	p := path.Join(*root, student, project)
	info, err := os.Stat(p)
	if err != nil {
		logger.Notice("could not open " + p + "; assuming no records")
		records = "No commits recorded"
		return
	}

	r, found := d[student]
	if !found {
		d[student] = new(record)
	} else if r.records.timestamp.Equal(info.ModTime()) {
		return r.records.contents
	}

	_records, err := ioutil.ReadFile(p)
	if err != nil {
		logger.Notice("could not open " + p + "; assuming no records")
		records = "No commits recorded"
	} else {
		records  = `<table summary="Records" class="booktabs">`
		records += `<tr><th class="booktabs">Commit</th><th class="booktabs">Time</th><th class="booktabs">Result</th></tr>`
		records += string(_records)
		records += "</table>"
		d[student].records.contents  = records
		d[student].records.timestamp = info.ModTime()
	}

	return
}

/* Returns a mapping of project names to "done" or "failed". */
func (d dbFilesystem) attempts(student string) (result map[string]string, err error) {
	dir    := "/www/" + student + "/"
	suffix := "-records"
	result  = make(map[string]string)

	files, err := ioutil.ReadDir(dir)
	if os.IsNotExist(err) {
		logger.Notice(dir + " does not yet exist; no submission results")
		return result, nil
	}
	if err != nil {
		return
	}

	for _, info := range files {
		var f *os.File
		var ok bool

		if !strings.HasSuffix(info.Name(), suffix) {
			continue
		}

		proj := strings.TrimSuffix(info.Name(), suffix)

		f, err = os.Open(dir + info.Name())
		if os.IsNotExist(err) {
			logger.Notice(info.Name() + " does not yet exist; " +
			             "no submission result")
			err = nil
			continue
		}
		if err != nil {
			return
		}

		defer f.Close()

		scanner := bufio.NewScanner(f)
		for scanner.Scan() {
			if strings.Contains(scanner.Text(), "PASS") {
				ok = true
				break
			}
		}

		if ok {
			result[proj] = "done"
		} else {
			result[proj] = "failed"
		}
	}

	return
}
