package main

import (
	"encoding/json"
	"fmt"
	"net"
	"os"
	"os/exec"
	"os/user"
	"testing"
	"time"
	"strings"
)

func TestMain(m *testing.M) {
	os.Remove(SOCKPATH)

	cmd := exec.Command("./queue")
	err := cmd.Start()
	if err != nil {
		return
	}

	time.Sleep(1 * time.Second)

	result := m.Run()

	err = cmd.Process.Kill()
	if err != nil {
		return
	}

	os.Exit(result)
}

func do(s string) (result result, err error) {
	var buf [1024]byte

	c, err := net.DialUnix("unix", nil, &net.UnixAddr{SOCKPATH, "unix"})
	if err != nil {
		return
	}
	defer c.Close()

	n, err := c.Write([]byte(s + "\n"))
	if err != nil {
		return
	}

	n, err = c.Read(buf[:])
	if err != nil {
		return
	}

	dec := json.NewDecoder(strings.NewReader(string(buf[:n])))
	err = dec.Decode(&result)

	return
}

const GRADER     = "/usr/sbin/grader"
const INITIALIZE = "/usr/sbin/aquinas-initialize-projects"
const ADD        = "/usr/sbin/aquinas-add-student-slave"
const CHECK      = "/usr/sbin/aquinas-get-ssh-authorized-keys"
const DEPLOY     = "/usr/sbin/aquinas-deploy-key"
const REMOVE     = "/usr/sbin/aquinas-remove-student-slave"

func TestCheckGraderAny(t *testing.T) {
	prog :=  GRADER
	user := "anyuser"
	if _, ok := checkPerm(prog, user); !ok {
		t.Error("check denied " + prog + " to " + user)
	}
}

func TestCheckInitializeProjectsMike(t *testing.T) {
	prog :=  INITIALIZE
	user := "mike"
	if _, ok := checkPerm(prog, user); ok {
		t.Error("check allowed " + prog + " to " + user)
	}
}

func TestCheckInitializeProjectsHttp(t *testing.T) {
	prog :=  INITIALIZE
	user := "http"
	if _, ok := checkPerm(prog, user); ok {
		t.Error("check allowed " + prog + " to " + user)
	}
}

func TestCheckInitializeProjectsTeacher(t *testing.T) {
	prog :=  INITIALIZE
	user := "teacher"
	if _, ok := checkPerm(prog, user); !ok {
		t.Error("check denied " + prog + " to " + user)
	}
}

func TestCheckAddMike(t *testing.T) {
	prog :=  ADD
	user := "mike"
	if _, ok := checkPerm(prog, user); ok {
		t.Error("check allowed " + prog + " to " + user)
	}
}

func TestCheckAddTeacher(t *testing.T) {
	prog :=  ADD
	user := "teacher"
	if _, ok := checkPerm(prog, user); ok {
		t.Error("check allowed " + prog + " to " + user)
	}
}

func TestCheckAddHttp(t *testing.T) {
	prog :=  ADD
	user := "http"
	if _, ok := checkPerm(prog, user); !ok {
		t.Error("check denied " + prog + " to " + user)
	}
}

func TestCheckCheckMike(t *testing.T) {
	prog :=  CHECK
	user := "mike"
	if _, ok := checkPerm(prog, user); ok {
		t.Error("check allowed " + prog + " to " + user)
	}
}

func TestCheckCheckTeacher(t *testing.T) {
	prog :=  CHECK
	user := "teacher"
	if _, ok := checkPerm(prog, user); ok {
		t.Error("check allowed " + prog + " to " + user)
	}
}

func TestCheckCheckHttp(t *testing.T) {
	prog :=  CHECK
	user := "http"
	if _, ok := checkPerm(prog, user); !ok {
		t.Error("check denied " + prog + " to " + user)
	}
}

func TestCheckDeployMike(t *testing.T) {
	prog :=  DEPLOY
	user := "mike"
	if _, ok := checkPerm(prog, user); ok {
		t.Error("check allowed " + prog + " to " + user)
	}
}

func TestCheckDeployTeacher(t *testing.T) {
	prog :=  DEPLOY
	user := "teacher"
	if _, ok := checkPerm(prog, user); ok {
		t.Error("check allowed " + prog + " to " + user)
	}
}

func TestCheckDeployHttp(t *testing.T) {
	prog :=  DEPLOY
	user := "http"
	if _, ok := checkPerm(prog, user); !ok {
		t.Error("check denied " + prog + " to " + user)
	}
}

func TestCheckRemoveMike(t *testing.T) {
	prog :=  REMOVE
	user := "mike"
	if _, ok := checkPerm(prog, user); ok {
		t.Error("check allowed " + prog + " to " + user)
	}
}

func TestCheckRemoveTeacher(t *testing.T) {
	prog :=  REMOVE
	user := "teacher"
	if _, ok := checkPerm(prog, user); ok {
		t.Error("check allowed " + prog + " to " + user)
	}
}

func TestCheckRemoveHttp(t *testing.T) {
	prog :=  REMOVE
	user := "http"
	if _, ok := checkPerm(prog, user); !ok {
		t.Error("check denied " + prog + " to " + user)
	}
}

func TestQueueDenied(t *testing.T) {
	var result result

	program := "/usr/sbin/aquinas-initialize-projects"

	result, err := do(program)
	if err != nil {
		t.Error(err.Error())
	}

	u, err := user.Current()
	if err != nil {
		t.Error(err.Error())
	}

	expected := fmt.Sprintf("%s run %s as %s: denied\n", u.Username, program, "")
	received := string(result.Stderr)
	if expected != received {
		t.Error("exprected " + expected + ", got " + received)
	}
}

func TestQueueInvalid(t *testing.T) {
	var result result

	program := "/disallowed/project"

	result, err := do(program)
	if err != nil {
		t.Error(err.Error())
	}

	u, err := user.Current()
	if err != nil {
		t.Error(err.Error())
	}

	expected := fmt.Sprintf("%s run %s as %s: denied\n", u.Username, program, "")
	received := string(result.Stderr)
	if expected != received {
		t.Error("exprected " + expected + ", got " + received)
	}
}
