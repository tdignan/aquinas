package main

/*
 * This program implements much of the hook executed when a student checks in a
 * project submission. The calling script must run this program as teacher using
 * sudo.
 *
 * (1) Use git to clone or pull the latest project descriptions.
 * (2) ssh to user@aq-test to invoke buildrunsh, provide project description,
 *     and receive results. Grade results and submit a record for use on WWW.
 *     Because this runs user-submitted code, it must run in that user's
 *     context.
 */

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"log/syslog"
	"os"
	"os/user"
	"path"
	"regexp"
	"strconv"
	"strings"
)

const WORKDIR  = "workdir"
const STUDENTS = "students"
const PROJECTS = "projects"
const RECORDS  = "records"
const USER_HOST   = "aquinas-user"
const REMOTE_HOME = "/home"

var logger *syslog.Writer

func getUser() (realUser, effectiveUser *user.User, err error) {
	if realUser, err = user.Current(); err != nil {
		return
	}

	effectiveUser, err = user.LookupId(strconv.Itoa(os.Geteuid()))
	return
}

func splitBaseLang(projName string) (projBase, projLang string) {
	amd64     := "AMD64"
	amd64Exp  := regexp.MustCompile(`.*` + amd64)
	c         := "C"
	cExp      := regexp.MustCompile(`.*` + c)
	golang    := "Go"
	goExp      := regexp.MustCompile(`.*` + golang)
	python    := "Python"
	pythonExp := regexp.MustCompile(`.*` + python)

	switch {
	case amd64Exp.MatchString(projName):
		projLang = amd64
		projBase = strings.TrimSuffix(projName, projLang)
	case cExp.MatchString(projName):
		projLang = c
		projBase = strings.TrimSuffix(projName, projLang)
	case goExp.MatchString(projName):
		projLang = golang
		projBase = strings.TrimSuffix(projName, projLang)
	case pythonExp.MatchString(projName):
		projLang = python
		projBase = strings.TrimSuffix(projName, projLang)
	default:
		projLang = ""
		projBase = projName
	}

	return
}

func grade(proj project, projBase, projLang, student, workdir string, results results) (pass bool) {
	pass = true

	if len(results.Results) != len(proj.Checks) {
		logger.Debug(fmt.Sprintf("results count is %d, not %d",
					  len(results.Results),
		                          len(proj.Checks)))
		pass = false
		return
	}

	for i, check := range proj.Checks {
		if !bytes.Equal(results.Results[i].Stdout, check.Stdout) {
			logger.Debug(fmt.Sprintf("stdout check %d failed: %s",
			                          i, results.Results[i].Stdout))
			pass = false
		}

		if !bytes.Equal(results.Results[i].Stderr, check.Stderr) {
			logger.Debug(fmt.Sprintf("stderr check %d failed: %s",
			                          i, results.Results[i].Stderr))
			pass = false
		}

		if results.Results[i].ExitCode != check.ExitCode {
			logger.Debug(fmt.Sprintf("exit code check %d failed: %d",
			                          i, results.Results[i].ExitCode))
			pass = false
		}
	}

	return
}

func record(proj project, projBase, projLang, student, workdir string, results results, pass bool) (err error, msg string) {
	recordDir := path.Join(workdir, RECORDS, projBase)

	recordPath := path.Join(recordDir, student + projLang + ".record")
	recordFile, err := os.OpenFile(recordPath,
				       os.O_APPEND | os.O_CREATE | os.O_WRONLY,
				       0600)
	if err != nil {
		return
	}

	defer recordFile.Close()

	projGit := path.Join(workdir, "students", student, projBase + projLang, ".git")
	hash, _, err := run(nil, nil, "git", "--git-dir", projGit,
	                  "log", "-1", "--pretty=%H")
	if err != nil {
		return
	}
	results.Hash = string(hash)

	record := log.New(recordFile, "", log.LstdFlags)
	if err != nil {
		return
	}

	msg = student + " " + proj.Name + " " +
	       strings.TrimSuffix(results.Hash, "\n") + " "
	if pass {
		msg += "PASS"
	} else {
		msg += "FAIL"
	}

	record.Println(msg)

	if _, _, err = run(nil, nil, "git", "-C", recordDir, "add", recordPath); err != nil {
		return
	}

	gitMsg := "Append record: " + msg
	if _, _, err = run(nil, nil, "git", "-C", recordDir, "commit", "-m", gitMsg); err != nil {
		return
	}

	if _, _, err = run(nil, nil, "git", "-C", recordDir, "push"); err != nil {
		return
	}

	return
}

/* projName is "helloC", proj.Name is "hello". */
func deploySubmission(student, projLang, projName string, proj project,
                      effectiveUser *user.User) (err error) {
	studentUser, err := user.Lookup(normalizeUsername(student))
	if err != nil {
		return
	}

	logger.Debug("will deploy solution to " + studentUser.HomeDir)

	workdir := path.Join(effectiveUser.HomeDir, WORKDIR)
	if _, err = os.Stat(workdir); os.IsNotExist(err) {
		if err = os.Mkdir(workdir, 0700); err != nil {
			return
		}
	}

	logger.Debug(workdir + " (now) exists")

	workdir = path.Join(workdir, STUDENTS)
	if _, err = os.Stat(workdir); os.IsNotExist(err) {
		if err = os.Mkdir(workdir, 0700); err != nil {
			return
		}
	}

	logger.Debug(workdir + " (now) exists")

	workdir = path.Join(workdir, student)
	if _, err = os.Stat(workdir); os.IsNotExist(err) {
		if err = os.Mkdir(workdir, 0700); err != nil {
			return
		}
	}

	logger.Debug(workdir + " (now) exists")

	/* Remove existing repo to avoid problems related to force pushes. */
	if err = os.RemoveAll(path.Join(workdir, projName)); err != nil {
		return
	}

	repo := path.Join(studentUser.HomeDir, projName)
	if err = updateRepo(repo, "master", workdir, projName); err != nil {
		return
	}

	logger.Debug("updated repository " + repo + " at " + workdir)

	srcPath := path.Join(workdir, projName)
	remoteHome := path.Join(REMOTE_HOME, student)
	if _, _, err = run(nil, nil, "scp", "-r", srcPath,
	                  "root@" + USER_HOST + "." + conf.Domain + ":" +
	                   remoteHome); err != nil {
		return
	}

	logger.Debug("copied " + srcPath + " to " + remoteHome)

	if _, altmain := proj.Altmain[projLang]; altmain {
		driverPath := path.Join(effectiveUser.HomeDir, WORKDIR,
		                       "projects", proj.Name, "main2" + langToExt2(projLang))

		if _, err = os.Stat(driverPath); os.IsNotExist(err) {
			return
		}

		targetPath := path.Join(remoteHome, projName)

		if _, _, err = run(nil, nil, "scp", "-r", driverPath,
				  "root@" + USER_HOST + "." + conf.Domain + ":" +
				   targetPath); err != nil {
			return
		}
	}

	if _, _, err = runRemote(nil, nil, "root@" + USER_HOST + "." + conf.Domain,
	                        "chown", "-R",
	                         normalizeUsername(student) + ":" +
	                         normalizeUsername(student),
	                         remoteHome); err != nil {
		return
	}

	return
}

func main() {
	var err error
	var pass bool
	var ran bool
	var proj project
	var realUser, effectiveUser *user.User

	logger, err = syslog.New(syslog.LOG_NOTICE, os.Args[0])
	if err != nil {
		panic(err)
	}

	logger.Debug("executing")

	student  := os.Args[1]
	projName := os.Args[2]

	logger.Debug("received request to test project " + projName +
	             " for " + student)

	if realUser, effectiveUser, err = getUser(); err != nil {
		fail(logger.Err, err.Error())
	}

	logger.Debug(fmt.Sprintf("executing user is %s, effective user is %s",
	                          realUser.Username,
	                          effectiveUser.Username))

	projBase, projLang := splitBaseLang(projName)

	projDir  := path.Join(effectiveUser.HomeDir, WORKDIR, RECORDS, projBase)
	if _, err = os.Stat(projDir); os.IsNotExist(err) {
		if err = os.Mkdir(projDir, 0700); err != nil {
			fail(logger.Err, err.Error())
		}
	}

	logger.Debug(projDir + " (now) exists")

	lastPath := path.Join(projDir, student + projLang + ".last")
	lastFile, err := os.OpenFile(lastPath, os.O_CREATE | os.O_TRUNC | os.O_WRONLY, 0600)
	if err != nil {
		fail(logger.Err, err.Error())
	}
	defer lastFile.Close()

	logger.Debug(lastPath + " (now) exists")

	repo := path.Join(effectiveUser.HomeDir, PROJECTS)
	workdir := path.Join(effectiveUser.HomeDir, WORKDIR)
	if err := updateRepo(repo, "master", workdir, PROJECTS); err != nil {
                lastFile.WriteString("system error: grader: could not update projects repository\n")
		fail(logger.Err, err.Error())
	}

	logger.Debug("updated repository " + repo + " at " + workdir)

	descPath := path.Join(workdir, PROJECTS, projBase, "description.json")
	projJson, err := os.Open(descPath)
	if err != nil {
                lastFile.WriteString("system error: grader: could not open project description\n")
		fail(logger.Err, err.Error())
	}
	defer projJson.Close()

	logger.Debug("opened " + descPath)

	proj, err = decodeProject(projJson)
	if err != nil {
		fail(logger.Err, err.Error())
	}

	logger.Debug("decoded " + descPath)

	if _, err = projJson.Seek(0, 0); err != nil {
		fail(logger.Err, err.Error())
	}

	if err := deploySubmission(student, projLang, projName, proj, effectiveUser);
	          err != nil {
                lastFile.WriteString("system error: grader: could not deploy submission\n")
		fail(logger.Err, err.Error())
	}

	json := "\"" + projLang + "\""
	buildRunShInput := io.MultiReader(strings.NewReader(json), projJson)

	var results results
	userHost := normalizeUsername(student) + "@aquinas-user." + conf.Domain
	stdout, _, err := runSsh(buildRunShInput, nil, userHost)
	if err != nil {
		logger.Err(err.Error())
		lastFile.WriteString(err.Error())
		ran = false
	} else {
		lastFile.WriteString("No error\n")
		ran = true
	}

	logger.Debug("built and ran")

	results, err = decodeResults(bytes.NewReader(stdout))
	if err != nil {
                lastFile.WriteString("system error: grader: could not decode test results\n")
		fail(logger.Err, err.Error())
	}

	logger.Debug("decoded results")

	repo = path.Join(effectiveUser.HomeDir, RECORDS)
	if err := updateRepo(repo, "master", workdir, RECORDS); err != nil {
		fail(logger.Err, err.Error())
	}

	logger.Debug("updated repository " + repo + " at " + workdir)

	if ran {
		pass = grade(proj, projBase, projLang, student, workdir, results)
	}

	err, msg := record(proj, projBase, projLang, student, workdir, results, pass)
	if err != nil {
                lastFile.WriteString("system error: grader: could not record results\n")
		fail(logger.Err, err.Error())
	}

	if _, _, err = run(nil, nil, "aquinas-update-www", student, projBase, projLang); err != nil {
                lastFile.WriteString("system error: grader: could not update website\n")
		fail(logger.Err, err.Error())
	}

	logger.Debug(msg)
}
