Aquinas is an interactive learning system that aims to teach computer
programming and exploit development.  Teachers define programming
projects, and students complete the projects and submit their work using
Git. Aquinas provides a website which lists the projects and provides
a summary of each student's progress.

A number of goals drove the design of Aquinas:

1. Allow for projects which involve network programming and exploit
development.

2. Facilitate easy-to-define projects with a consistent specification
language.

3. Ease reuse of projects across many programming languages.

4. Allow for high-quality assignment instructions.

5. Provide a web- and Git-based interface to students.

6. Provide for automated grading and student feedback.

7. Apply the principle of least privilege and use a type-safe language.

Installing Aquinas
==================

Building and deploying Aquinas requires the following software:

    * make
    * The Go programming language
    * The jq JSON parser
    * Git
    * An ssh client
    * LaTeXML

Quick check
-----------

1. Run "make".

2. Run "(sudo) ./httpd -dummy -root ./www/".

You should now be able to browse to http://localhost/. The dynamic
aspects of Aquinas will not work without a full installation, but you
should be able to review most of Aquinas' look and feel using this
dummy mode.

Full installation
-----------------

1.  Edit aquinas.json and aquinas-build.json to facilitate installing
Aquinas at your site.

2.  Run "make all vms". This will build some utilities and the Aquinas VMs.

3.  Install each disk image (e.g., aquinas-git-openwrt-x86-64-combined-ext4.img)
and each domain configuration (e.g., vm-aquinas-git.cfg) so that they
can be run by your hypervisor.

4.  Update your DHCP service to provide the correct IP address to each
domain. Refer to each domain configuration for the host's MAC address.

5.  Update your DNS service to provide an appropriate A record for each
domain. Refer to each domain configuration for the host's name.

6.  Configure syslog-ng on each VM. The aquinas-user and aquinas-target
VMs must be configured to reference a log server by IP address, as their
firewall prohibits DNS queries.

7.  Start each configured VM.

8.  On your development computer, run "./aquinas-setup-ssh".

9.  On aquinas-www, run "sudo -u http aquinas-add-student test PASSWORD".

10. On your development computer, run "./aquinas-add-teacher
~/.ssh/id_rsa.pub".

11. Push a projects repository to aquinas-git:/mnt/xvdb/teacher/projects.

12. Push a records repository to aquinas-git:/mnt/xvdb/teacher/records.

13. Push the HTML documents to aquinas-www.

14. On your development computer and from directory test/, run
"./test-all."

Writing a project
=================

Writing a project is a matter of creating a machine-readable JSON file
to define the project and a LaTeX (or Markdown) fragment to instruct
students in how to complete the project.

Project definition
------------------

Here is the definition of a very simple project named *unix*. The
absence of the *checks* keyword means that Aquinas will not grade this
project. Because *languages* is set to *none*, Aquinas will generate no
language-specific variants of this project. Perhaps this project could
guide the student through an introduction to UNIX without requiring a
graded deliverable.

```
{
        "name": "unix",
        "languages": [ "none" ]
}
```

Here is another language-agnostic project. This project, *git*, assumes
the completion of *unix*. Aquinas will take this into account when
ordering the list of projects presented to a student.  This project
provides a check (*checks*); running the *command* *cat NOTES* from the
root of the student's Git repository should print "In case of fire: git
commit, git push, and leave the building!" to standard out. (I.e., the file
NOTES should exist in the Git repository and it should contain "In case
of fire: ...")

The value in the *stdout* field is this string, base64 encoded (but not
depicted in its entirety here). The base64 encoding is to allow such
values to contain binary data.

```
{
	"name": "git",
	"languages": [ "none" ],
	"prerequisites": [ "unix" ],
	"checks": [{ "command": "cat NOTES",
	             "stdin": null,
	             "stdout": "SW4gY2Fz...",
	             "stderr": null,
	             "exitCode": 0
	          }]
}
```

A student may submit the following project in C or Python. In the case of
C, the submission should contain *hello.c*, and this file should compile
to a program which prints "Hello, world\n". A Python submission should
take the form of *hello* as an executable script (i.e., with shebang). As
with the previous example, the value of *stdout* is base64 encoded to
support binary data.

```
{
	"name": "hello",
	"languages": [ "C", "Python" ],
	"prerequisites": [ "git" ],
	"checks": [{ "command": "./hello",
	             "stdin": null,
	             "stdout": "SGVsbG8sIHdvcmxkIQo=",
	             "stderr": null,
	             "exitCode": 0
	          }]
}
```

The following network project installs a service, defined in *network.go*,
on Aquinas' target host. A correct solution will interact with this
service as the project instructions prescribe to produce the output
required by the project's checks. The value of *service_port* minus its
extension must match the value of *name*, and every project must bear a
unique *service_port*.

```
{
        "name": "network",
        "languages": [ "C", "Go", "Python" ],
        "prerequisites": [ "hello" ],
        "checks": [{ "command": "./network",
                     "stdin": null,
                     "stdout": "...",
                     "stderr": null,
                     "exitCode": 0
                  }],
        "service_program": "network.go",
        "service_port": 1025
}
```

Sometimes it might be useful to directly call functions in a project
solution when testing. An *altmain* statement allows this. For example,
if *altmain* is defined as *true* for C, Aquinas will compile the
submitted program such that the function *main2* in the teacher-defined
file main2.c serves as the program's main function.

```
{
        "name": "functions",
        "languages": [ "C", "Go", "Python" ],
        "prerequisites": [ "hello" ],
        "altmain": { "C": true },
        "checks": [{ "command": "./functions",
                     "stdin": null,
                     "stdout": "...",
                     "stderr": null,
                     "exitCode": 0
                  }]
}
```

Project instructions
--------------------

Teachers write project instructions in the form of a LaTeX fragment,
which Aquinas combines with a template before processing into a
HTML document. It is a good practice to use `\section*` to provide
three sections: *Command* (or *Function*) *summary*, *Lesson*, and
*Assignment*. Aquinas will provide the prelude and epilog material;
here it is sufficient to begin with the first `\section*`.

Aquinas provides the following LaTeX commands for use in a project's
instructions:

=`\cmd`=
    Typeset the argument as a command.
=`\project`=
    Typeset the argument as a project name.
=`\conf`=
    Typeset the argument as a configuration file.
=`\file`=
    Typeset the argument as a file name.
=`\dir`=
    Typeset the argument as a directory name.
=`\fn`=
    Typeset the argument as a function name.
=`\host`=
    Typeset the argument as a host name.
=`\keypress`=
    Typeset the argument as if it were a key to be pressed.
=`\unix`=
    Typeset the work UNIX.
=`\shprompt`=
    Typeset a Bourne shell prompt.
=`\cmddesc`=
    Define a command within a LaTeX description list. Like `\item`, except
    Aquinas notes occurences of `\cmddesc` to produce a command reference
    page.
=`\fncdesc`=
    Define a C function within a LaTeX description list. Like `\item`, except
    Aquinas notes occurences of `\fncdesc` to produce a C function reference
    page. Variants exist for other languages such as Go (`\fngodesc`)
    and Python (`\fnpythondesc`).
=`\ifC`=
    If the language associated with the current project variant matches
    (here the C variant), then include the subsequent text up until the
    next `\fi`. Otherwise print nothing.
=`\ifshebang`=
    If the language can make use of a shebang, then print the second argument.
    Otherwise print nothing.
=`\clone`=
    Provide instructions on how to clone the current project using Git.
=`\submission`=
    Print instructions on how to submit a project solution.
=`\submissionNoLanguage`=
    Print instructions on how to submit a language-agnostic project solution.

Alternatively, a teacher can describe a project using Markdown.

The Aquinas VMs
===============

=aquinas-www=
    The HTTP server which allows users to read project assignments and
    view submission results.

=aquinas-git=
    The Git server to which users make project submissions.

=aquinas-user=
    The host which runs project submissions during the grading process.

=aquinas-target=
    The host which runs network services with which user programs might
    interact.

System inputs
=============

=(unauthenticated) http://aquinas-www/landing.html=
    Allows a user to either log in or register.

=(unauthenticated) http://aquinas-www/login.html=
    Accepts student email and password. Allows a student to log in to
    the web interface. Transitions state of HTTP session to authenticated.

=(unauthenticated) http://aquinas-www/register.html=
    Accepts an email address. Allows a registering student to initiate
    the registration process. Sends an email to the registering student
    which allows him to complete the registration.

=(unauthenticated) http://aquinas-www/register3.html=
    Accepts an email address, nonce, hashed token, and password. Allows
    a registering student to prove ownership of an email address and
    thus complete the registration process.

=(authenticated) http://aquinas-www/index.html=
    Allows a student to select a project page.

=(authenticated) http://aquinas-www/p.html, where p is a project=
    Allows a student to view information which describes project p.

=ssh://s@aquinas-git/mnt/xvdb/s/p, where s is a student and p is a project=
    Interact with student s's project p submission using Git/git-shell.
    Git hook invokes grader, run as teacher, with s and p as its input.

=ssh://t@aquinas-git/mnt/xvdb/teacher/projects, where t is a teacher=
    Interact with the project definitions using Git/git-shell.  Git hook
    invokes aquinas-initialize-projects, run as root.

=ssh://t@aquinas-git/mnt/xvdb/teacher/records, where t is a teacher=
    Interact with the project submission records using Git/git-shell.

=ssh://root@*=
    Allows developers shell access.

Sudo permissions
================

aquinas-git
-----------

* All users can run grader as teacher
* Teacher can run aquinas-initialize-projects as root
* Http can run aquinas-add-student-slave as root
* Http can run aquinas-get-ssh-authorized-keys as root
* Http can run aquinas-deploy-key as root
* Http can run aquinas-remove-student-slave as root

SSH keys
========

=root@aquinas-git=
    generated by openwrt-build

=teacher@aquinas-git=
    generated by openwrt-build

=http@aquinas-www=
    generated by openwrt-build

=Developers=
    installed by hand

=Teachers=
    installed by aquinas-add-teacher

=Students=
    installed by ssh/ssh2.html

Permitted SSH connections
=========================

From               |To                  |Installed by        |Purpose
-------------------|--------------------|--------------------|-----------------------------
Developers         |root@aquinas-git    |openwrt-build/manual|Development/administration
http@aquinas-www   |http@aquinas-git    |setup-ssh           |Httpd uses to run httpsh:
                   |                    |                    |  check for student
                   |                    |                    |  check for SSH key
                   |                    |                    |  add a new student
                   |                    |                    |  deploy SSH key
                   |                    |                    |  remove a student
Developers         |test@aquinas-git    |test case           |Pushing solutions during
                   |                    |                    |tests
Teachers           |teacher@aquinas-git |add-teacher         |Project deployment
Developers         |root@aquinas-user   |openwrt-build/manual|Development
teacher@aquinas-git|root@aquinas-user   |setup-ssh           |Used by initialize-project
                   |                    |                    |to pull host key and user
                   |                    |                    |key from aquinas-user
                   |                    |                    |(Cannot use test@, because
                   |                    |                    |test has shell set to
                   |                    |                    |buildrunsh)
                   |                    |                    |Also used by grader to place
                   |                    |                    |user-submitted code on
                   |                    |                    |aquinas-user
root@aquinas-git   |root@aquinas-user   |setup-ssh           |Add or remove user.
root@aquinas-git   |teacher@aquinas-user|setup-ssh           |Compile network project services.
teacher@aquinas-git|test@aquinas-user   |add-student         |Grader uses to run buildrunsh
                   |                    |                    |on user VM; Git hook runs as
                   |                    |                    |user, and uses sudo to run
                   |                    |                    |grader as teacher
Developers         |root@aquinas-www    |openwrt-build/manual|Development/administration
Teachers           |teacher@aquinas-www |add-teacher         |Updating HTML documents
teacher@aquinas-git|teacher@aquinas-www |setup-ssh           |Used by grader to update
                   |                    |                    |records on aquinas-www
Students           |STUDENT@aquinas-git |ssh/ssh2.html       |Commit submissions to Git
root@aquinas-git   |root@aquinas-target |setup-ssh           |Deploy network project services.

Component isolation
===================

The design of Aquinas isolates its components to prevent student
program from exfiltrating data available to them during the grading
process. This prevents student knowledge of the checks which grade
their submissions. Furthermore, student programs cannot interact beyond
the aquinas-user host with the exception of allowed connections to
aquinas-target.

Firewall rules
--------------

The host firewalls on aquinas-user and aquinas-target prohibit all
outgoing connections with the exception of a connection to a syslog
server. All interaction with these hosts is by way of incoming SSH
connections or project-specific services. The aim of this is to prevent
a user-written program from exfiltrating data from either host while
executing for the purpose of grading.

Chroot jails
------------

Services on aquinas-target run as the user nobody and within a chroot
jail.
