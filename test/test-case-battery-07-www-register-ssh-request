#!/bin/sh

. ./test-functions

log_host=root@golem.$domain
log_path=$root/var/log/messages
student=test@example.com
tmp=$(mktemp)
rand=$RANDOM

trap "/bin/rm -rf $tmp" SIGTERM SIGINT EXIT
trap "ssh root@aquinas-www.$domain sudo -u http aquinas-remove-student $student >/dev/null 2>&1" SIGTERM SIGINT EXIT

# Do not capture error; just cleaning up/should not already exist.
ssh root@aquinas-www.$domain sudo -u http aquinas-remove-student $student >/dev/null 2>&1

# This should fail due to example.com domain. See httpd.go which
# is written to log what we need, but not proceed further in the
# process.
error=$(wget --quiet -O /dev/null --post-data "student=$student" http://aquinas-www.$domain/register2.html)
if [ $? != 8 ]; then
	failmsg "$0" ${error:-no stderr}
fi

# NOTE: will fail if more than 1000 log messages between above and below.

log_msg=$(ssh $log_host tail -n 1000 $log_path | grep -a "preparing register email student: $student" | tail -1)
failbad $? "$0" "could not find \"preparing register email student: $student\" in log"

nonce=$(echo $log_msg | awk '{ print $12 }')
token=$(echo $log_msg | awk '{ print $14 }')
password=$(echo $log_msg | awk '{ print $16 }')

# Register a new account.
error=$(wget --quiet -O "$tmp" --post-data "student=$student&nonce=$nonce&token=$token&password=$password" http://aquinas-www.$domain/register3.html)
failbad $? "$0" ${error:-no stderr}

uuid=$(grep jobUuid "$tmp" | cut -d "\"" -f 4)
failbad $? "$0" ${uuid:-no stderr}

error=$(waitlog 600 root@golem.flyn.org "$uuid" /mnt/xvdb/var/log/messages 2>&1)
failbad $? "$0" ${error:-no stderr}

# Flush httpd's cache of SSH authorized_keys status.
error=$(ssh root@aquinas-www.$domain /etc/init.d/httpd restart)
failbad $? "$0" ${error:-no stderr}

# Ensure we get prompted for SSH key until we submit one.
error=$(wget --quiet -O "$tmp" --http-user=test@example.com --http-password=$password --auth-no-challenge http://aquinas-www.$domain/)
failbad $? "$0" ${error:-no stderr}

grep -q "Before you can complete your first project" $tmp
failbad $? "$0" "\"Before you can complete your first project\" missing from HTML"

passmsg "$0" okay
