. ../aquinas-functions

msg() {
	echo -n "${1}: "
	shift
	echo -n "${1}: "
	shift
	echo "${*}"
}

failmsg() {
	msg "[ ] FAIL" "$@"
	exit 1
}

passmsg() {
	msg "[+] PASS" "$@"
	exit 0
}

passmsg2() {
	msg "[+] PASS" "$@"
}

failbad() {
	exitcode="$1"
	shift
	name="$1"
	shift
	string="$*"
	if [ "$exitcode" != 0 ]; then
		failmsg "$name" "$exitcode: $string"
	fi
}

failgood() {
	exitcode="$1"
	shift
	if [ "$exitcode" = 0 ]; then
		exitcode=1
	else
		exitcode=0
	fi
	failbad $exitcode "$@"
}

testcommit() {
	testname="$1"
	proj="$2"
	lang="$3"
	src="$4"
	prog="$5"
	pass_value="$6"
	last_value="$7"

	dir=$(mktemp -d)
	rand=$RANDOM

	trap "/bin/rm -rf $dir" SIGTERM SIGINT EXIT

	error=$(git clone -q test@aquinas-git.$domain:$root/test/$proj$lang $dir 2>&1)
	failbad $? "$testname" "clone failed: ${error:-no stderr}"

	pushd $dir >/dev/null

	if [ -z "$src" ]; then
		echo "$prog" | sed "s/\$rand/$rand/g" >$proj
		# Python or another shebang language.
		chmod +x $proj
		git add $proj
	else
		echo "$prog" | sed "s/\$rand/$rand/g" >$src
		git add $src
	fi

	git commit -q -s -m "Another revision: $rand" >/dev/null

	uuid=$(git push -q 2>&1 | cut -d " " -f 2)
	failbad $? "$testname" "gathering UUID failed: ${uuid:-no stderr}"

	error=$(waitlog 600 root@golem.flyn.org "$uuid" /mnt/xvdb/var/log/messages 2>&1)
	failbad $? "$testname" "waitlog failed: ${error:-no stderr}"

	hash=$(git log -1 --pretty=%H)

	popd >/dev/null

	record=$root/teacher/workdir/records/$proj/test$lang.record
	error=$(ssh root@aquinas-git.$domain grep -q $hash.*\"$pass_value\" $record 2>&1)
	failbad $? "$testname" "pass value wrong: ${error:-no stderr}"

	last=$root/teacher/workdir/records/$proj/test$lang.last
	error=$(ssh root@aquinas-git.$domain grep -q \"$last_value\" $last 2>&1)
	failbad $? "$testname" "last value failed: ${error:-no stderr}"
}

urlencode() {
	local length="${#1}"
	for (( i = 0; i < length; i++ )); do
		local c="${1:i:1}"
		case $c in
		[a-zA-Z0-9.~_-]) printf "$c" ;;
		*) printf '%s' "$c" | xxd -p -c1 |
		   while read c; do printf '%%%s' "$c"; done ;;
		esac
	done
}

waitlog() {
	local timeout="$1"
	local userserver="$2"
	local regexp="$3"
	local log="$4"

	timeout "$timeout" ssh "$userserver" tail -n 1000 -f \"$log\" \| grep -ql \"$regexp\"
}
