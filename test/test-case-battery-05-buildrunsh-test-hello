#!/bin/sh

# NOTE: We run this test twice to test the case of a fresh clone and a
# pull. Aquinas-user.$domain's buildrunsh executes a clone the first time
# we run this. It runs a pull the second time. Hence the symbolic link to
# this file which results in two test executions.

. ./test-functions

dir=$(mktemp -d)

trap "/bin/rm -rf $dir" SIGTERM SIGINT EXIT

error=$(git clone -q test@aquinas-git.$domain:$root/test/helloC $dir 2>&1)
failbad $? "$0" ${error:-no stderr}

pushd $dir >/dev/null

cat >hello.c <<EOF
#include <stdio.h>

int main(int argc, char *argv[]){
	printf("Hello, world!\n");
}
EOF

git add hello.c
git commit -q -s -m "Initial commit" >/dev/null

error=$(scp -r $dir root@aquinas-user.$domain:/home/test/helloC 2>&1)
failbad $? "$0" ${error:-no stderr}

error=$(ssh root@aquinas-user.$domain chown -R test:test /home/test/helloC 2>&1)
failbad $? "$0" ${error:-no stderr}

popd >/dev/null

request='"C"{
	"name": "hello",
	"prerequisites": [ "git" ],
	"languages": [ "C", "Python" ],
	"checks": [{ "command": "./hello",
	             "stdin": null,
	             "stdout": "SGVsbG8sIHdvcmxkIQo=",
	             "stderr": null,
	             "exitCode": 0
	          }]
}'
expected='{"results":[{"command":"./hello","stdout":"SGVsbG8sIHdvcmxkIQo=","stderr":null,"exitCode":0}],"hash":'
received=$(ssh -T root@aquinas-git.$domain <<EOF
echo '$request' | sudo -u teacher ssh -T test@aquinas-user.$domain
EOF
)

# Use regex to avoid having to predict trailing hash=...
case "$received" in
"$expected"*)
	;;
*)
	error="$expected not in $received"
	failbad 1 "$0" ${error:-no stderr}
	;;
esac

passmsg "$0" okay
